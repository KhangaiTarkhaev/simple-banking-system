package ru.khangai.simplebankingapp.dao;

import org.springframework.stereotype.Repository;
import ru.khangai.simplebankingapp.entities.Counterparty;

import javax.persistence.EntityManager;
import javax.persistence.LockModeType;
import java.util.ArrayList;
import java.util.List;

@Repository
public class CounterpartyDaoImpl implements CounterpartyDao {

    private final EntityManager em;

    public CounterpartyDaoImpl(EntityManager em) {
        this.em = em;
    }

    @Override
    public Counterparty bindCounterparties(Long counterpartyId, Long anotherCounterpartyId) {
        Counterparty counterparty = em.find(Counterparty.class, counterpartyId, LockModeType.OPTIMISTIC);
        Counterparty counterpartyToAdd = em.find(Counterparty.class, anotherCounterpartyId, LockModeType.OPTIMISTIC);
        bind(counterpartyToAdd, counterparty);
        bind(counterparty, counterpartyToAdd);
        return counterpartyToAdd;
    }

    private void bind(Counterparty counterparty, Counterparty anotherCounterparty) {
        if (anotherCounterparty.getCounterparties() == null) {
            anotherCounterparty.setCounterparties(new ArrayList<>());
        }
        anotherCounterparty.getCounterparties().add(counterparty);
    }

    @Override
    public List<Counterparty> getAllCounterpartyOf(Long counterpartyId) {
        return em.createQuery("SELECT c FROM Counterparty c JOIN c.counterpartyOf cntp WHERE cntp.id=:counterpartyId", Counterparty.class)
                .setLockMode(LockModeType.OPTIMISTIC)
                .setParameter("counterpartyId", counterpartyId)
                .getResultList();
    }

    @Override
    public Counterparty findById(Long counterpartyId) {
        return em.find(Counterparty.class, counterpartyId, LockModeType.OPTIMISTIC);
    }

}
