package ru.khangai.simplebankingapp.dao;

import ru.khangai.simplebankingapp.entities.Counterparty;

import java.util.Collection;

public interface CounterpartyDao {

    Counterparty bindCounterparties(Long id, Long anotherCounterpartyId);

    Collection<Counterparty> getAllCounterpartyOf(Long userId);

    Counterparty findById(Long counterpartyId);

}
