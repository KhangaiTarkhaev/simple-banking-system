package ru.khangai.simplebankingapp.dao;

import org.springframework.stereotype.Repository;
import ru.khangai.simplebankingapp.entities.User;

import javax.persistence.EntityManager;
import javax.persistence.LockModeType;
import javax.persistence.PersistenceContext;

@Repository
public class UserDaoImpl implements UserDao {

    @PersistenceContext
    private final EntityManager em;

    public UserDaoImpl(EntityManager em) {
        this.em = em;
    }

    @Override
    public User findById(Long id) {
        return em.find(User.class, id, LockModeType.OPTIMISTIC);
    }

    @Override
    public User save(User user) {
        em.persist(user);
        return user;
    }
}
