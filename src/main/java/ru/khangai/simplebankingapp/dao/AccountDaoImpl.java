package ru.khangai.simplebankingapp.dao;

import org.springframework.stereotype.Repository;
import ru.khangai.simplebankingapp.entities.Account;

import javax.persistence.EntityManager;
import javax.persistence.LockModeType;
import javax.persistence.PersistenceContext;
import java.math.BigDecimal;

@Repository
public class AccountDaoImpl implements AccountDao {

    @PersistenceContext
    private final EntityManager em;

    public AccountDaoImpl(EntityManager em) {
        this.em = em;
    }

    @Override
    public Account findById(Long id) {
        return em.find(Account.class, id, LockModeType.OPTIMISTIC);
    }

    @Override
    public Account save(Account account) {
        em.persist(account);
        return account;
    }

    @Override
    public Account income(Long accountId, BigDecimal amount) {
        String jpql = "UPDATE Account a SET a.amount=a.amount + :amount WHERE a.id=:accountId";
        return changeAmount(accountId, amount, jpql);
    }

    @Override
    public Account outcome(Long accountId, BigDecimal amount) {
        String jpql = "UPDATE Account a SET a.amount=a.amount - :amount WHERE a.id=:accountId";
        return changeAmount(accountId, amount, jpql);
    }

    private Account changeAmount(Long accountId, BigDecimal amount, String jpql) {
        int rows = em.createQuery(jpql)
                .setParameter("accountId", accountId)
                .setParameter("amount", amount)
                .executeUpdate();
        if (rows < 1) {
            throw new IllegalArgumentException("Cannot update amount, possibly account not exists");
        } else {
            return em.getReference(Account.class, accountId);
        }
    }
}
