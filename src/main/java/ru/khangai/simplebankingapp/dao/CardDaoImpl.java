package ru.khangai.simplebankingapp.dao;

import org.springframework.stereotype.Repository;
import ru.khangai.simplebankingapp.entities.Card;

import javax.persistence.EntityManager;
import javax.persistence.LockModeType;
import javax.persistence.PersistenceContext;
import java.math.BigDecimal;
import java.util.Collection;

@Repository
public class CardDaoImpl implements CardDao {

    @PersistenceContext
    private final EntityManager em;

    public CardDaoImpl(EntityManager em) {
        this.em = em;
    }

    @Override
    public Collection<Card> getAllCardsByUserId(Long userId) {
        return em.createQuery("SELECT c FROM Card c WHERE c.cardHolder.id=:userId", Card.class)
                .setLockMode(LockModeType.OPTIMISTIC)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Override
    public Card save(Card card) {
        em.persist(card);
        return card;
    }

    @Override
    public BigDecimal getCardAmountById(Long cardId) {
        return em.createQuery("SELECT c.account.amount FROM Card c WHERE c.id=:cardId", BigDecimal.class)
                .setLockMode(LockModeType.OPTIMISTIC)
                .setParameter("cardId", cardId)
                .getSingleResult();
    }

    @Override
    public Card findByCardNumber(String cardNumber) {
        return em.createQuery("SELECT c FROM Card c WHERE c.cardNumber=:cardNumber", Card.class)
                .setLockMode(LockModeType.OPTIMISTIC)
                .setParameter("cardNumber", cardNumber)
                .getSingleResult();
    }
}
