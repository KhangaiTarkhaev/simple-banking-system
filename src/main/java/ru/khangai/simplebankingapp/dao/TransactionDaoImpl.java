package ru.khangai.simplebankingapp.dao;

import org.springframework.stereotype.Repository;
import ru.khangai.simplebankingapp.entities.transaction.Transaction;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Repository
public class TransactionDaoImpl implements TransactionDao {

    @PersistenceContext
    private final EntityManager em;

    public TransactionDaoImpl(EntityManager em) {
        this.em = em;
    }

    @Override
    public <T extends Transaction> T save(T transaction) {
        em.persist(transaction);
        return transaction;
    }
}
