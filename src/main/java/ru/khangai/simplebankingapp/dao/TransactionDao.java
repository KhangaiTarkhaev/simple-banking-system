package ru.khangai.simplebankingapp.dao;

import ru.khangai.simplebankingapp.entities.transaction.Transaction;

public interface TransactionDao {

    <T extends Transaction> T save(T transaction);

}
