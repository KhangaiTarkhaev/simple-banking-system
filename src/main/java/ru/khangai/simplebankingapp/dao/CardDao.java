package ru.khangai.simplebankingapp.dao;

import ru.khangai.simplebankingapp.entities.Card;

import java.math.BigDecimal;
import java.util.Collection;

public interface CardDao {

    Collection<Card> getAllCardsByUserId(Long userId);

    Card save(Card card);

    BigDecimal getCardAmountById(Long cardId);

    Card findByCardNumber(String cardNumber);
}
