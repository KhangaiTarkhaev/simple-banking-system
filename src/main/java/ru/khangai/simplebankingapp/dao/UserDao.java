package ru.khangai.simplebankingapp.dao;

import ru.khangai.simplebankingapp.entities.User;

public interface UserDao {

    User findById(Long id);

    User save(User user);

}
