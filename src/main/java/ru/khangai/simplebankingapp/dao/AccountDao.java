package ru.khangai.simplebankingapp.dao;

import ru.khangai.simplebankingapp.entities.Account;

import java.math.BigDecimal;

public interface AccountDao {

    Account findById(Long id);

    Account save(Account account);

    Account income(Long enrollmentAccountId, BigDecimal amount);

    Account outcome(Long withdrawalAccountId, BigDecimal amount);
}
