package ru.khangai.simplebankingapp.converters;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Service;
import ru.khangai.simplebankingapp.dto.PersonDetailsDto;
import ru.khangai.simplebankingapp.entities.PersonDetails;

@Service
public class PersonDetailsToDtoConverter implements Converter<PersonDetails, PersonDetailsDto> {

    @Override
    public PersonDetailsDto convert(PersonDetails source) {
        return new PersonDetailsDto()
                .setFirstName(source.getFirstName())
                .setLastName(source.getLastName())
                .setEmail(source.getEmail())
                .setPhoneNumber(source.getPhoneNumber());
    }

}
