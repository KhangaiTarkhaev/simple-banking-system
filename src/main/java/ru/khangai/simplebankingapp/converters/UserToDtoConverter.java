package ru.khangai.simplebankingapp.converters;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Service;
import ru.khangai.simplebankingapp.dto.PersonDetailsDto;
import ru.khangai.simplebankingapp.dto.counterparty.UserDto;
import ru.khangai.simplebankingapp.entities.PersonDetails;
import ru.khangai.simplebankingapp.entities.User;

@Service
public class UserToDtoConverter implements Converter<User, UserDto> {

    private final Converter<PersonDetails, PersonDetailsDto> personDetailsPersonDetailsDtoConverter;

    public UserToDtoConverter(Converter<PersonDetails, PersonDetailsDto> personDetailsPersonDetailsDtoConverter) {
        this.personDetailsPersonDetailsDtoConverter = personDetailsPersonDetailsDtoConverter;
    }

    @Override
    public UserDto convert(User source) {
        return new UserDto()
                .setId(source.getId())
                .setPersonDetailsDto(personDetailsPersonDetailsDtoConverter.convert(source.getDetails()));
    }
}
