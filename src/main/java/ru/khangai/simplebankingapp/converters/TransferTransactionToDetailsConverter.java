package ru.khangai.simplebankingapp.converters;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Service;
import ru.khangai.simplebankingapp.dto.transactions.TransactionDetails;
import ru.khangai.simplebankingapp.entities.operations.IncomeOperation;
import ru.khangai.simplebankingapp.entities.operations.OutcomeOperation;
import ru.khangai.simplebankingapp.entities.transaction.TransferTransaction;

import static ru.khangai.simplebankingapp.dto.transactions.TransactionDetails.Type.TRANSFER;

@Service
public class TransferTransactionToDetailsConverter implements Converter<TransferTransaction, TransactionDetails> {

    @Override
    public TransactionDetails convert(TransferTransaction source) {
        IncomeOperation enrollmentOperation = source.getEnrollmentOperation();
        OutcomeOperation withdrawalOperation = source.getWithdrawalOperation();
        return new TransactionDetails().setId(source.getId())
                .setAmount(enrollmentOperation.getAmount())
                .setType(TRANSFER)
                .setOutcomeAccountId(withdrawalOperation.getAccount().getId())
                .setIncomeAccountId(enrollmentOperation.getAccount().getId())
                .setTimestamp(source.getTimestamp());
    }
}
