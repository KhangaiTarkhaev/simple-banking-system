package ru.khangai.simplebankingapp.converters;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Service;
import ru.khangai.simplebankingapp.dto.transactions.TransactionDetails;
import ru.khangai.simplebankingapp.entities.operations.IncomeOperation;
import ru.khangai.simplebankingapp.entities.transaction.IncomeTransaction;

import static ru.khangai.simplebankingapp.dto.transactions.TransactionDetails.Type.INCOME;

@Service
public class IncomeTransactionToDetailsConverter implements Converter<IncomeTransaction, TransactionDetails> {

    @Override
    public TransactionDetails convert(IncomeTransaction source) {
        IncomeOperation incomeOperation = source.getIncomeOperation();
        return new TransactionDetails()
                .setId(source.getId())
                .setAmount(incomeOperation.getAmount())
                .setType(INCOME)
                .setIncomeAccountId(incomeOperation.getAccount().getId())
                .setTimestamp(source.getTimestamp());
    }
}
