package ru.khangai.simplebankingapp.converters;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Service;
import ru.khangai.simplebankingapp.dto.cards.CardDto;
import ru.khangai.simplebankingapp.entities.Card;

@Service
public class CardToDtoConverter implements Converter<Card, CardDto> {

    @Override
    public CardDto convert(Card source) {
        return new CardDto()
                .setCardNumber(source.getCardNumber())
                .setCardHolderName(source.getCardHolderName())
                .setCvcCode(source.getCvcCode());
    }

}
