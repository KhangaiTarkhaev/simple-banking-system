package ru.khangai.simplebankingapp.converters;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Service;
import ru.khangai.simplebankingapp.dto.transactions.TransactionDetails;
import ru.khangai.simplebankingapp.entities.operations.OutcomeOperation;
import ru.khangai.simplebankingapp.entities.transaction.OutcomeTransaction;

import static ru.khangai.simplebankingapp.dto.transactions.TransactionDetails.Type.OUTCOME;

@Service
public class OutcomeTransactionToDetailsConverter implements Converter<OutcomeTransaction, TransactionDetails> {

    @Override
    public TransactionDetails convert(OutcomeTransaction source) {
        OutcomeOperation outcomeOperation = source.getOutcomeOperation();
        return new TransactionDetails()
                .setId(source.getId())
                .setAmount(outcomeOperation.getAmount())
                .setType(OUTCOME)
                .setOutcomeAccountId(outcomeOperation.getAccount().getId())
                .setTimestamp(source.getTimestamp());
    }
}
