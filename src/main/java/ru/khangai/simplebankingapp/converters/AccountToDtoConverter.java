package ru.khangai.simplebankingapp.converters;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Service;
import ru.khangai.simplebankingapp.dto.AccountDto;
import ru.khangai.simplebankingapp.entities.Account;

@Service
public class AccountToDtoConverter implements Converter<Account, AccountDto> {

    @Override
    public AccountDto convert(Account source) {
        return new AccountDto()
                .setId(source.getId())
                .setAmount(source.getAmount())
                .setCounterpartyId(source.getCounterparty().getId());
    }
}
