package ru.khangai.simplebankingapp.services;

import ru.khangai.simplebankingapp.dto.AccountDto;
import ru.khangai.simplebankingapp.entities.Account;

public interface AccountService {

    AccountDto createNewAccount(Long counterpartyId);

    Account getById(Long id);

}
