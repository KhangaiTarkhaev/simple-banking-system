package ru.khangai.simplebankingapp.services.cards;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.khangai.simplebankingapp.dao.CardDao;
import ru.khangai.simplebankingapp.dto.cards.CardAmountDto;
import ru.khangai.simplebankingapp.dto.cards.CardCreationCredentials;
import ru.khangai.simplebankingapp.dto.cards.CardDto;
import ru.khangai.simplebankingapp.dto.cards.CardOperationCredentials;
import ru.khangai.simplebankingapp.dto.transactions.ExternalIncomeTransactionCredentials;
import ru.khangai.simplebankingapp.dto.transactions.ExternalOutcomeTransactionCredentials;
import ru.khangai.simplebankingapp.dto.transactions.TransactionDetails;
import ru.khangai.simplebankingapp.dto.transactions.TransferTransactionCredentials;
import ru.khangai.simplebankingapp.entities.Card;
import ru.khangai.simplebankingapp.services.transactions.TransactionService;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CardServiceImpl implements CardService {

    private final CardCreationService cardCreationService;

    private final Converter<Card, CardDto> cardCardDtoConverter;

    private final CardDao cardDao;

    private final TransactionService transactionService;

    public CardServiceImpl(CardCreationService cardCreationService, Converter<Card, CardDto> cardCardDtoConverter,
                           CardDao cardDao, TransactionService transactionService) {
        this.cardCreationService = cardCreationService;
        this.cardCardDtoConverter = cardCardDtoConverter;
        this.cardDao = cardDao;
        this.transactionService = transactionService;
    }

    @Override
    @Transactional
    public CardDto createNewCard(CardCreationCredentials cardCreationCredentials) {
        Card newCard = cardCreationService.createNewCard(cardCreationCredentials);
        return cardCardDtoConverter.convert(newCard);
    }

    @Override
    @Transactional(readOnly = true)
    public List<CardDto> getAllCardsByUserId(Long userId) {
        Collection<Card> cards = cardDao.getAllCardsByUserId(userId);
        return cards.stream().map(cardCardDtoConverter::convert).collect(Collectors.toList());
    }

    @Override
    @Transactional(readOnly = true)
    public CardAmountDto getCardBalance(CardDto cardDto) {
        String cardNumber = cardDto.getCardNumber();
        Card card = cardDao.findByCardNumber(cardNumber);
        checkCVC(cardDto.getCvcCode(), card.getCvcCode());
        return new CardAmountDto().setAmount(card.getAccount().getAmount());
    }

    private void checkCVC(String actual, String expected) {
        if (!expected.equals(actual)) throw new IllegalArgumentException("Wrong CVC");
    }

    @Override
    @Transactional
    public TransactionDetails deposit(CardOperationCredentials cardOperationCredentials) {
        CardDto cardDto = cardOperationCredentials.getEnrollmentCard();
        Card card = cardDao.findByCardNumber(cardDto.getCardNumber());
        checkCVC(cardDto.getCvcCode(), card.getCvcCode());
        return transactionService.proceedTransaction(new ExternalIncomeTransactionCredentials()
                .setAmount(cardOperationCredentials.getAmount())
                .setEnrollmentAccountId(card.getAccount().getId()));
    }

    @Override
    @Transactional
    public TransactionDetails withdraw(CardOperationCredentials cardOperationCredentials) {
        CardDto cardDto = cardOperationCredentials.getWithdrawalCard();
        Card card = cardDao.findByCardNumber(cardDto.getCardNumber());
        checkCVC(cardDto.getCvcCode(), card.getCvcCode());
        return transactionService.proceedTransaction(new ExternalOutcomeTransactionCredentials()
                .setAmount(cardOperationCredentials.getAmount())
                .setWithdrawalAccountId(card.getAccount().getId()));
    }

    @Override
    @Transactional
    public TransactionDetails transfer(CardOperationCredentials transferCredentials) {
        CardDto withdrawalCardDto = transferCredentials.getWithdrawalCard();
        CardDto enrollmentCardDto = transferCredentials.getEnrollmentCard();
        Card withdrawalCard = cardDao.findByCardNumber(withdrawalCardDto.getCardNumber());
        Card enrollmentCard = cardDao.findByCardNumber(enrollmentCardDto.getCardNumber());
        checkCVC(enrollmentCardDto.getCvcCode(), enrollmentCard.getCvcCode());
        checkCVC(withdrawalCardDto.getCvcCode(), withdrawalCard.getCvcCode());
        return transactionService.proceedTransaction(new TransferTransactionCredentials()
                .setAmount(transferCredentials.getAmount())
                .setEnrollmentAccountId(enrollmentCard.getAccount().getId())
                .setWithdrawalAccountId(withdrawalCard.getAccount().getId()));
    }
}
