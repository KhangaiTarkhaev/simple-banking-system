package ru.khangai.simplebankingapp.services.cards;

import ru.khangai.simplebankingapp.dto.cards.CardCreationCredentials;
import ru.khangai.simplebankingapp.entities.Card;

public interface CardCreationService {

    Card createNewCard(CardCreationCredentials credentials);

}
