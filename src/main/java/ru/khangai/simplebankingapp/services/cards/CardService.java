package ru.khangai.simplebankingapp.services.cards;

import ru.khangai.simplebankingapp.dto.cards.CardAmountDto;
import ru.khangai.simplebankingapp.dto.cards.CardCreationCredentials;
import ru.khangai.simplebankingapp.dto.cards.CardDto;
import ru.khangai.simplebankingapp.dto.cards.CardOperationCredentials;
import ru.khangai.simplebankingapp.dto.transactions.TransactionDetails;

import java.util.List;

public interface CardService {

    CardDto createNewCard(CardCreationCredentials cardCreationCredentials);

    List<CardDto> getAllCardsByUserId(Long userId);

    CardAmountDto getCardBalance(CardDto cardDto);

    TransactionDetails deposit(CardOperationCredentials depositCredentials);

    TransactionDetails withdraw(CardOperationCredentials withdrawCredentials);

    TransactionDetails transfer(CardOperationCredentials transferCredentials);
}
