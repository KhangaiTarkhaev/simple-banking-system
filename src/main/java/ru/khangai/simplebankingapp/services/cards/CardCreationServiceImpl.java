package ru.khangai.simplebankingapp.services.cards;

import org.springframework.stereotype.Service;
import ru.khangai.simplebankingapp.dao.CardDao;
import ru.khangai.simplebankingapp.dao.UserDao;
import ru.khangai.simplebankingapp.dto.cards.CardCreationCredentials;
import ru.khangai.simplebankingapp.entities.Account;
import ru.khangai.simplebankingapp.entities.Card;
import ru.khangai.simplebankingapp.entities.PersonDetails;
import ru.khangai.simplebankingapp.entities.User;
import ru.khangai.simplebankingapp.services.AccountService;

import static ru.khangai.simplebankingapp.services.cards.RandomDigitsGenerator.generateRandomDigits;

@Service
public class CardCreationServiceImpl implements CardCreationService {

    private final AccountService accountService;

    private final UserDao userDao;

    private final CardDao cardDao;

    public CardCreationServiceImpl(AccountService accountService,
                                   UserDao userDao,
                                   CardDao cardDao) {
        this.accountService = accountService;
        this.userDao = userDao;
        this.cardDao = cardDao;
    }

    public Card createNewCard(CardCreationCredentials cardCreationCredentials) {
        Account account = accountService.getById(cardCreationCredentials.getAccountId());
        Long counterpartyId = account.getCounterparty().getId();
        User user = userDao.findById(counterpartyId);
        PersonDetails personDetails = user.getDetails();
        Card card = new Card()
                .setAccount(account)
                .setCardHolder(user)
                .setCardHolderName(personDetails.getFirstName().toUpperCase() + " " + personDetails.getLastName().toUpperCase())
                .setCvcCode(String.valueOf(generateRandomDigits(3)))
                .setCardNumber(String.valueOf(generateRandomDigits(16)));
        return cardDao.save(card);
    }
}
