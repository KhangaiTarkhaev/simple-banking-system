package ru.khangai.simplebankingapp.services.cards;

import java.util.concurrent.ThreadLocalRandom;

public class RandomDigitsGenerator {

    private RandomDigitsGenerator() {
    }

    public static long generateRandomDigits(int length) {
        long m = (long) Math.pow(10, (double) length - (double) 1);
        return m + ThreadLocalRandom.current().nextLong(9 * m);
    }
}
