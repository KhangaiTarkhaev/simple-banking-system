package ru.khangai.simplebankingapp.services.counterparty;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Service;
import ru.khangai.simplebankingapp.dto.counterparty.UserDto;
import ru.khangai.simplebankingapp.entities.User;

@Service
public class CounterpartyConverterVisitorImpl implements CounterpartyConverterVisitor {

    private final Converter<User, UserDto> userUserDtoConverter;

    public CounterpartyConverterVisitorImpl(Converter<User, UserDto> userUserDtoConverter) {
        this.userUserDtoConverter = userUserDtoConverter;
    }

    @Override
    public UserDto convert(User counterparty) {
        return userUserDtoConverter.convert(counterparty);
    }

}
