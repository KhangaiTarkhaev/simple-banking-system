package ru.khangai.simplebankingapp.services.counterparty;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.khangai.simplebankingapp.dao.CounterpartyDao;
import ru.khangai.simplebankingapp.dto.counterparty.CounterpartyCredentials;
import ru.khangai.simplebankingapp.dto.counterparty.CounterpartyDto;
import ru.khangai.simplebankingapp.entities.Counterparty;

import java.util.Collection;
import java.util.stream.Collectors;

@Service
public class CounterpartyServiceImpl implements CounterpartyService {

    private final CounterpartyDao counterpartyDao;

    private final CounterpartyConverterVisitor converterVisitor;

    public CounterpartyServiceImpl(CounterpartyDao counterpartyDao,
                                   CounterpartyConverterVisitor converterVisitor) {
        this.counterpartyDao = counterpartyDao;
        this.converterVisitor = converterVisitor;
    }

    @Transactional
    @Override
    public CounterpartyDto addNewCounterparty(CounterpartyCredentials credentials) {
        Counterparty addedCounterparty = counterpartyDao.bindCounterparties(credentials.getId(), credentials.getCounterpartyIdToBind());
        return addedCounterparty.convertToCounterpartyDto(converterVisitor);
    }

    @Transactional(readOnly = true)
    @Override
    public Collection<CounterpartyDto> getAllCounterpartiesOf(Long userId) {
        Collection<Counterparty> counterparties = counterpartyDao.getAllCounterpartyOf(userId);
        return counterparties.stream().map(counterparty -> counterparty.convertToCounterpartyDto(converterVisitor)).collect(Collectors.toList());
    }

}
