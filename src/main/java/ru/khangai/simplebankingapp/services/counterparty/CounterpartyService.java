package ru.khangai.simplebankingapp.services.counterparty;

import ru.khangai.simplebankingapp.dto.counterparty.CounterpartyCredentials;
import ru.khangai.simplebankingapp.dto.counterparty.CounterpartyDto;

import java.util.Collection;

public interface CounterpartyService {

    CounterpartyDto addNewCounterparty(CounterpartyCredentials credentials);

    Collection<CounterpartyDto> getAllCounterpartiesOf(Long userId);

}
