package ru.khangai.simplebankingapp.services.counterparty;

import ru.khangai.simplebankingapp.dto.counterparty.UserDto;
import ru.khangai.simplebankingapp.entities.User;

public interface CounterpartyConverterVisitor {

    UserDto convert(User counterparty);

}
