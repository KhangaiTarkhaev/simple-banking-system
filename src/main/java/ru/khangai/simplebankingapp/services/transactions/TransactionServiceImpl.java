package ru.khangai.simplebankingapp.services.transactions;

import org.springframework.stereotype.Service;
import ru.khangai.simplebankingapp.dto.transactions.TransactionCredentials;
import ru.khangai.simplebankingapp.dto.transactions.TransactionDetails;
import ru.khangai.simplebankingapp.entities.transaction.Transaction;

import javax.transaction.Transactional;

@Service
public class TransactionServiceImpl implements TransactionService {

    private final TransactionBuilder builderVisitor;

    private final TransactionExecutor executorVisitor;

    public TransactionServiceImpl(TransactionBuilder builderVisitor, TransactionExecutor executorVisitor) {
        this.builderVisitor = builderVisitor;
        this.executorVisitor = executorVisitor;
    }

    @Override
    @Transactional
    public TransactionDetails proceedTransaction(TransactionCredentials credentials) {
        Transaction transaction = credentials.createTransaction(builderVisitor);
        return transaction.execute(executorVisitor);
    }
}
