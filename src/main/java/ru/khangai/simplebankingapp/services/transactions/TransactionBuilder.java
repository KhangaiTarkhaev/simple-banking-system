package ru.khangai.simplebankingapp.services.transactions;

import ru.khangai.simplebankingapp.dto.transactions.ExternalIncomeTransactionCredentials;
import ru.khangai.simplebankingapp.dto.transactions.ExternalOutcomeTransactionCredentials;
import ru.khangai.simplebankingapp.dto.transactions.TransferTransactionCredentials;
import ru.khangai.simplebankingapp.entities.transaction.IncomeTransaction;
import ru.khangai.simplebankingapp.entities.transaction.OutcomeTransaction;
import ru.khangai.simplebankingapp.entities.transaction.TransferTransaction;

public interface TransactionBuilder {

    IncomeTransaction build(ExternalIncomeTransactionCredentials transactionCredentials);

    OutcomeTransaction build(ExternalOutcomeTransactionCredentials transactionCredentials);

    TransferTransaction build(TransferTransactionCredentials transactionCredentials);

}
