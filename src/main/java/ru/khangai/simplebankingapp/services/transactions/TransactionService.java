package ru.khangai.simplebankingapp.services.transactions;

import ru.khangai.simplebankingapp.dto.transactions.TransactionCredentials;
import ru.khangai.simplebankingapp.dto.transactions.TransactionDetails;

public interface TransactionService {

    TransactionDetails proceedTransaction(TransactionCredentials credentials);

}
