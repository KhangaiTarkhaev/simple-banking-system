package ru.khangai.simplebankingapp.services.transactions;

import ru.khangai.simplebankingapp.dto.transactions.TransactionDetails;
import ru.khangai.simplebankingapp.entities.transaction.IncomeTransaction;
import ru.khangai.simplebankingapp.entities.transaction.OutcomeTransaction;
import ru.khangai.simplebankingapp.entities.transaction.TransferTransaction;

public interface TransactionExecutor {

    TransactionDetails execute(IncomeTransaction incomeTransaction);

    TransactionDetails execute(OutcomeTransaction outcomeTransaction);

    TransactionDetails execute(TransferTransaction transferTransaction);
}
