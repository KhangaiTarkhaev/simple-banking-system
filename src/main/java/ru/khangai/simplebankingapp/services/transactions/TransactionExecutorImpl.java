package ru.khangai.simplebankingapp.services.transactions;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Service;
import ru.khangai.simplebankingapp.dao.AccountDao;
import ru.khangai.simplebankingapp.dao.TransactionDao;
import ru.khangai.simplebankingapp.dto.transactions.TransactionDetails;
import ru.khangai.simplebankingapp.entities.operations.IncomeOperation;
import ru.khangai.simplebankingapp.entities.operations.OutcomeOperation;
import ru.khangai.simplebankingapp.entities.transaction.IncomeTransaction;
import ru.khangai.simplebankingapp.entities.transaction.OutcomeTransaction;
import ru.khangai.simplebankingapp.entities.transaction.TransferTransaction;


@Service
public class TransactionExecutorImpl implements TransactionExecutor {

    private final TransactionDao transactionDao;

    private final AccountDao accountDao;

    private final Converter<IncomeTransaction, TransactionDetails> incomeTransactionTransactionDetailsConverter;

    private final Converter<OutcomeTransaction, TransactionDetails> outcomeTransactionTransactionDetailsConverter;

    private final Converter<TransferTransaction, TransactionDetails> transferTransactionTransactionDetailsConverter;

    public TransactionExecutorImpl(TransactionDao transactionDao, AccountDao accountDao, Converter<IncomeTransaction, TransactionDetails> incomeTransactionTransactionDetailsConverter, Converter<OutcomeTransaction, TransactionDetails> outcomeTransactionTransactionDetailsConverter, Converter<TransferTransaction, TransactionDetails> transferTransactionTransactionDetailsConverter) {
        this.transactionDao = transactionDao;
        this.accountDao = accountDao;
        this.incomeTransactionTransactionDetailsConverter = incomeTransactionTransactionDetailsConverter;
        this.outcomeTransactionTransactionDetailsConverter = outcomeTransactionTransactionDetailsConverter;
        this.transferTransactionTransactionDetailsConverter = transferTransactionTransactionDetailsConverter;
    }

    @Override
    public TransactionDetails execute(IncomeTransaction incomeTransaction) {
        executeIncomeOperation(incomeTransaction.getIncomeOperation());
        incomeTransaction = transactionDao.save(incomeTransaction);
        return incomeTransactionTransactionDetailsConverter.convert(incomeTransaction);
    }

    @Override
    public TransactionDetails execute(TransferTransaction transferTransaction) {
        executeOutcomeOperation(transferTransaction.getWithdrawalOperation());
        executeIncomeOperation(transferTransaction.getEnrollmentOperation());
        transferTransaction = transactionDao.save(transferTransaction);
        return transferTransactionTransactionDetailsConverter.convert(transferTransaction);
    }

    @Override
    public TransactionDetails execute(OutcomeTransaction outcomeTransaction) {
        executeOutcomeOperation(outcomeTransaction.getOutcomeOperation());
        outcomeTransaction = transactionDao.save(outcomeTransaction);
        return outcomeTransactionTransactionDetailsConverter.convert(outcomeTransaction);
    }

    private void executeIncomeOperation(IncomeOperation incomeOperation) {
        Long accountId = incomeOperation.getAccount().getId();
        accountDao.income(accountId, incomeOperation.getAmount());
    }

    private void executeOutcomeOperation(OutcomeOperation outcomeOperation) {
        Long accountId = outcomeOperation.getAccount().getId();
        accountDao.outcome(accountId, outcomeOperation.getAmount());
    }
}
