package ru.khangai.simplebankingapp.services.transactions;

import org.springframework.stereotype.Component;
import ru.khangai.simplebankingapp.dto.transactions.*;
import ru.khangai.simplebankingapp.entities.Account;
import ru.khangai.simplebankingapp.entities.operations.IncomeOperation;
import ru.khangai.simplebankingapp.entities.operations.OutcomeOperation;
import ru.khangai.simplebankingapp.entities.transaction.IncomeTransaction;
import ru.khangai.simplebankingapp.entities.transaction.OutcomeTransaction;
import ru.khangai.simplebankingapp.entities.transaction.Transaction;
import ru.khangai.simplebankingapp.entities.transaction.TransferTransaction;

@Component
public class TransactionBuilderImpl implements TransactionBuilder {

    @Override
    public IncomeTransaction build(ExternalIncomeTransactionCredentials transactionCredentials) {
        IncomeTransaction incomeTransaction = new IncomeTransaction();
        incomeTransaction.setIncomeOperation(getEnrollmentOperation(transactionCredentials, incomeTransaction));
        return incomeTransaction;
    }

    @Override
    public OutcomeTransaction build(ExternalOutcomeTransactionCredentials transactionCredentials) {
        OutcomeTransaction outcomeTransaction = new OutcomeTransaction();
        outcomeTransaction.setOutcomeOperation(getWithdrawalOperation(transactionCredentials, outcomeTransaction));
        return outcomeTransaction;
    }

    @Override
    public TransferTransaction build(TransferTransactionCredentials transactionCredentials) {
        TransferTransaction transaction = new TransferTransaction();
        transaction.setEnrollmentOperation(getEnrollmentOperation(transactionCredentials, transaction))
                .setWithdrawalOperation(getWithdrawalOperation(transactionCredentials, transaction));
        return transaction;
    }

    private IncomeOperation getEnrollmentOperation(IncomeTransactionCredentials transactionCredentials, Transaction transaction) {
        return new IncomeOperation()
                .setTransaction(transaction)
                .setAccount(new Account()
                        .setId(transactionCredentials.getEnrollmentAccountId()))
                .setAmount(transactionCredentials.getAmount());
    }

    private OutcomeOperation getWithdrawalOperation(OutcomeTransactionCredentials transactionCredentials, Transaction transaction) {
        return new OutcomeOperation()
                .setTransaction(transaction)
                .setAccount(new Account()
                        .setId(transactionCredentials.getWithdrawalAccountId()))
                .setAmount(transactionCredentials.getAmount());
    }
}
