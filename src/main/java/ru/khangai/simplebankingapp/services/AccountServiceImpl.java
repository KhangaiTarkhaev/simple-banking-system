package ru.khangai.simplebankingapp.services;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.khangai.simplebankingapp.dao.AccountDao;
import ru.khangai.simplebankingapp.dao.CounterpartyDao;
import ru.khangai.simplebankingapp.dto.AccountDto;
import ru.khangai.simplebankingapp.entities.Account;
import ru.khangai.simplebankingapp.entities.Counterparty;

import java.math.BigDecimal;

@Service
public class AccountServiceImpl implements AccountService {

    private final AccountDao accountDao;

    private final CounterpartyDao counterpartyDao;

    private final Converter<Account, AccountDto> accountDtoConverter;

    public AccountServiceImpl(AccountDao accountDao, CounterpartyDao counterpartyDao, Converter<Account, AccountDto> accountDtoConverter) {
        this.accountDao = accountDao;
        this.counterpartyDao = counterpartyDao;
        this.accountDtoConverter = accountDtoConverter;
    }

    @Override
    @Transactional
    public AccountDto createNewAccount(Long counterpartyId) {
        Counterparty counterparty = counterpartyDao.findById(counterpartyId);
        Account account = new Account()
                .setAmount(BigDecimal.ZERO)
                .setCounterparty(counterparty);
        account = accountDao.save(account);
        return accountDtoConverter.convert(account);
    }

    @Override
    @Transactional(readOnly = true)
    public Account getById(Long id) {
        return accountDao.findById(id);
    }
}
