package ru.khangai.simplebankingapp.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.khangai.simplebankingapp.dto.cards.CardAmountDto;
import ru.khangai.simplebankingapp.dto.cards.CardCreationCredentials;
import ru.khangai.simplebankingapp.dto.cards.CardDto;
import ru.khangai.simplebankingapp.dto.cards.CardOperationCredentials;
import ru.khangai.simplebankingapp.dto.transactions.TransactionDetails;
import ru.khangai.simplebankingapp.services.cards.CardService;

import javax.validation.Valid;
import java.util.Collection;
import java.util.List;

import static org.springframework.http.HttpStatus.CREATED;
import static ru.khangai.simplebankingapp.dto.cards.CardOperationCredentials.CardOperationType;
import static ru.khangai.simplebankingapp.dto.cards.CardOperationCredentials.CardOperationType.DEPOSIT;
import static ru.khangai.simplebankingapp.dto.cards.CardOperationCredentials.CardOperationType.WITHDRAW;

@RequestMapping("/cards")
@RestController
public class CardController {

    public final CardService cardService;

    public CardController(CardService cardService) {
        this.cardService = cardService;
    }

    @PostMapping
    public ResponseEntity<CardDto> createNewCard(@RequestBody @Valid CardCreationCredentials cardCreationCredentials) {
        CardDto cardDto = cardService.createNewCard(cardCreationCredentials);
        return ResponseEntity.status(CREATED).body(cardDto);
    }

    @GetMapping
    public ResponseEntity<Collection<CardDto>> getCardList(@RequestParam Long counterpartyId) {
        List<CardDto> cards = cardService.getAllCardsByUserId(counterpartyId);
        return ResponseEntity.ok(cards);
    }

    @PostMapping("/balance")
    public ResponseEntity<CardAmountDto> checkBalance(@RequestBody @Valid CardDto balanceCheckCredentials) {
        CardAmountDto cardBalance = cardService.getCardBalance(balanceCheckCredentials);
        return ResponseEntity.ok(cardBalance);
    }

    @PostMapping("/transactions")
    public ResponseEntity<TransactionDetails> executeCardOperation(@RequestBody @Valid CardOperationCredentials cardOperationCredentials) {
        CardOperationType type = cardOperationCredentials.getType();
        TransactionDetails details;
        if (type == DEPOSIT) {
            details = cardService.deposit(cardOperationCredentials);
        } else if (type == WITHDRAW) {
            details = cardService.withdraw(cardOperationCredentials);
        } else {
            details = cardService.transfer(cardOperationCredentials);
        }
        return ResponseEntity.ok(details);
    }

}
