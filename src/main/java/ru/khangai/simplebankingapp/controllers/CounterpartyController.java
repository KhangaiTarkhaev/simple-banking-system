package ru.khangai.simplebankingapp.controllers;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.khangai.simplebankingapp.dto.counterparty.CounterpartyCredentials;
import ru.khangai.simplebankingapp.dto.counterparty.CounterpartyDto;
import ru.khangai.simplebankingapp.dto.counterparty.MultipleCounterpartyResponse;
import ru.khangai.simplebankingapp.services.counterparty.CounterpartyService;

import javax.validation.Valid;
import java.util.Collection;

@RestController
@RequestMapping("/users/counterparties")
public class CounterpartyController {

    private final CounterpartyService counterpartyService;

    public CounterpartyController(CounterpartyService customerService) {
        this.counterpartyService = customerService;
    }

    @PostMapping
    public ResponseEntity<CounterpartyDto> addNewCounterparty(@RequestBody @Valid CounterpartyCredentials credentials) {
        CounterpartyDto addedCounterparty = counterpartyService.addNewCounterparty(credentials);
        return ResponseEntity.ok(addedCounterparty);
    }

    @GetMapping
    public ResponseEntity<MultipleCounterpartyResponse> getAllCounterparty(@RequestParam Long counterpartyId) {
        Collection<CounterpartyDto> counterpartyDtos = counterpartyService.getAllCounterpartiesOf(counterpartyId);
        return ResponseEntity.ok(new MultipleCounterpartyResponse().setCounterparties(counterpartyDtos));
    }

}
