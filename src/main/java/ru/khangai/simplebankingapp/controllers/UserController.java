package ru.khangai.simplebankingapp.controllers;

import org.springframework.core.convert.converter.Converter;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.khangai.simplebankingapp.dao.UserDao;
import ru.khangai.simplebankingapp.dto.PersonDetailsDto;
import ru.khangai.simplebankingapp.dto.counterparty.UserDto;
import ru.khangai.simplebankingapp.entities.PersonDetails;
import ru.khangai.simplebankingapp.entities.User;

import javax.transaction.Transactional;
import javax.validation.Valid;

import static org.springframework.http.HttpStatus.CREATED;

@RestController
@RequestMapping("/users")
public class UserController {

    private final Converter<User, UserDto> userUserDtoConverter;

    private final UserDao userDao;

    public UserController(Converter<User, UserDto> userUserDtoConverter, UserDao userDao) {
        this.userUserDtoConverter = userUserDtoConverter;
        this.userDao = userDao;
    }

    @PostMapping
    @Transactional
    public ResponseEntity<UserDto> createNewUser(@RequestBody @Valid UserDto userDto) {
        PersonDetailsDto personDetailsDto = userDto.getPersonDetailsDto();
        User save = userDao.save(new User()
                .setDetails(new PersonDetails()
                        .setFirstName(personDetailsDto.getFirstName())
                        .setLastName(personDetailsDto.getLastName())
                        .setEmail(personDetailsDto.getEmail())
                        .setPhoneNumber(personDetailsDto.getPhoneNumber())));
        return ResponseEntity.status(CREATED).body(userUserDtoConverter.convert(save));
    }

}
