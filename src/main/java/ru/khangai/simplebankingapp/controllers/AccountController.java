package ru.khangai.simplebankingapp.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.khangai.simplebankingapp.dto.AccountDto;
import ru.khangai.simplebankingapp.dto.IdWrapper;
import ru.khangai.simplebankingapp.services.AccountService;

import javax.validation.Valid;

import static org.springframework.http.HttpStatus.CREATED;

@RestController
@RequestMapping("/accounts")
public class AccountController {

    private final AccountService accountService;

    public AccountController(AccountService accountService) {
        this.accountService = accountService;
    }

    @PostMapping
    public ResponseEntity<AccountDto> addAccount(@RequestBody @Valid IdWrapper counterpartyId) {
        AccountDto newAccount = accountService.createNewAccount(counterpartyId.getId());
        return ResponseEntity.status(CREATED).body(newAccount);
    }

}
