package ru.khangai.simplebankingapp.controllers;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.khangai.simplebankingapp.dto.transactions.TransactionCredentials;
import ru.khangai.simplebankingapp.dto.transactions.TransactionDetails;
import ru.khangai.simplebankingapp.services.transactions.TransactionServiceImpl;

import javax.validation.Valid;

import static org.springframework.http.HttpStatus.CREATED;

@RestController
@RequestMapping("/transactions")
public class TransactionController {

    private final TransactionServiceImpl transactionService;

    public TransactionController(TransactionServiceImpl transactionService) {
        this.transactionService = transactionService;
    }

    @PostMapping
    public ResponseEntity<TransactionDetails> processTransaction(@RequestBody @Valid TransactionCredentials credentials) {
        TransactionDetails transactionDetails = transactionService.proceedTransaction(credentials);
        return ResponseEntity.status(CREATED).body(transactionDetails);
    }

}
