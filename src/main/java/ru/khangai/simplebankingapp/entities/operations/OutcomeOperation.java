package ru.khangai.simplebankingapp.entities.operations;

import ru.khangai.simplebankingapp.entities.Account;
import ru.khangai.simplebankingapp.entities.transaction.Transaction;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.math.BigDecimal;

@Entity
@DiscriminatorValue("OUTCOME")
public class OutcomeOperation extends Operation {

    @Override
    public OutcomeOperation setId(Long id) {
        this.id = id;
        return this;
    }

    @Override
    public OutcomeOperation setAccount(Account account) {
        this.account = account;
        return this;
    }

    @Override
    public OutcomeOperation setTransaction(Transaction transaction) {
        this.transaction = transaction;
        return this;
    }

    @Override
    public OutcomeOperation setAmount(BigDecimal amount) {
        this.amount = amount;
        return this;
    }

}
