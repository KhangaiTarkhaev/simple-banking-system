package ru.khangai.simplebankingapp.entities.operations;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.khangai.simplebankingapp.entities.Account;
import ru.khangai.simplebankingapp.entities.transaction.Transaction;

import javax.persistence.*;
import java.math.BigDecimal;

@Table(name = "operations")
@Entity
@Getter
@Setter
@Accessors(chain = true)
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "type",
        discriminatorType = DiscriminatorType.STRING)
public abstract class Operation {

    @Version
    private long version;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long id;

    @ManyToOne
    @JoinColumn(name = "account_id", nullable = false)
    protected Account account;

    @Column(name = "amount")
    protected BigDecimal amount;

    @OneToOne
    @JoinColumn(name = "transaction_id", nullable = false, referencedColumnName = "id")
    protected Transaction transaction;

}
