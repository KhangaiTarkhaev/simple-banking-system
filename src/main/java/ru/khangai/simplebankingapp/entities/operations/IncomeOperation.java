package ru.khangai.simplebankingapp.entities.operations;

import ru.khangai.simplebankingapp.entities.Account;
import ru.khangai.simplebankingapp.entities.transaction.Transaction;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.math.BigDecimal;

@Entity
@DiscriminatorValue("INCOME")
public class IncomeOperation extends Operation {

    @Override
    public IncomeOperation setId(Long id) {
        this.id = id;
        return this;
    }

    @Override
    public IncomeOperation setAccount(Account account) {
        this.account = account;
        return this;
    }

    @Override
    public IncomeOperation setTransaction(Transaction transaction) {
        this.transaction = transaction;
        return this;
    }

    @Override
    public IncomeOperation setAmount(BigDecimal amount) {
        this.amount = amount;
        return this;
    }
}
