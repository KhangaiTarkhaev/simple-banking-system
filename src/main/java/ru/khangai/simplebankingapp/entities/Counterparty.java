package ru.khangai.simplebankingapp.entities;

import lombok.Getter;
import lombok.Setter;
import ru.khangai.simplebankingapp.dto.counterparty.CounterpartyDto;
import ru.khangai.simplebankingapp.services.counterparty.CounterpartyConverterVisitor;

import javax.persistence.*;
import java.util.Collection;

@Getter
@Setter
@Entity
@Table
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public abstract class Counterparty {

    @Version
    private long version;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "counterparty_id_seq")
    @SequenceGenerator(
            name = "counterparty_id_seq",
            allocationSize = 1
    )
    @Column(name = "id", nullable = false)
    private Long id;

    @OneToMany(mappedBy = "counterparty")
    private Collection<Account> accounts;

    @ManyToMany
    @JoinTable(name = "counterparty_to_counterparty",
            joinColumns = @JoinColumn(name = "counterparty_id"),
            inverseJoinColumns = @JoinColumn(name = "counterparty_of_id")
    )
    private Collection<Counterparty> counterparties;

    @ManyToMany
    @JoinTable(name = "counterparty_to_counterparty",
            joinColumns = @JoinColumn(name = "counterparty_of_id"),
            inverseJoinColumns = @JoinColumn(name = "counterparty_id")
    )
    private Collection<Counterparty> counterpartyOf;

    public abstract CounterpartyDto convertToCounterpartyDto(CounterpartyConverterVisitor converterVisitor);

}
