package ru.khangai.simplebankingapp.entities;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.khangai.simplebankingapp.dto.counterparty.CounterpartyDto;
import ru.khangai.simplebankingapp.services.counterparty.CounterpartyConverterVisitor;

import javax.persistence.*;
import java.util.Collection;

@Entity
@Table(name = "users")
@Getter
@Setter
@Accessors(chain = true)
public class User extends Counterparty {

    @Embedded
    private PersonDetails details;

    @OneToMany(mappedBy = "cardHolder")
    private Collection<Card> cards;

    @Override
    public CounterpartyDto convertToCounterpartyDto(CounterpartyConverterVisitor converterVisitor) {
        return converterVisitor.convert(this);
    }
}
