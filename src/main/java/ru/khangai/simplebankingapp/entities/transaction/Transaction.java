package ru.khangai.simplebankingapp.entities.transaction;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.hibernate.annotations.CreationTimestamp;
import ru.khangai.simplebankingapp.dto.transactions.TransactionDetails;
import ru.khangai.simplebankingapp.services.transactions.TransactionExecutor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Getter
@Setter
@Accessors(chain = true)
@Table(name = "transactions")
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "type",
        discriminatorType = DiscriminatorType.STRING)
public abstract class Transaction {

    @Version
    private long version;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @CreationTimestamp
    private LocalDateTime timestamp;

    public abstract TransactionDetails execute(TransactionExecutor visitor);

    public LocalDateTime getTimestamp() {
        return timestamp;
    }
}


