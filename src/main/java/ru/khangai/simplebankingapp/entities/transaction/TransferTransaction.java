package ru.khangai.simplebankingapp.entities.transaction;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.khangai.simplebankingapp.dto.transactions.TransactionDetails;
import ru.khangai.simplebankingapp.entities.operations.IncomeOperation;
import ru.khangai.simplebankingapp.entities.operations.OutcomeOperation;
import ru.khangai.simplebankingapp.services.transactions.TransactionExecutor;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.OneToOne;

@Getter
@Setter
@Accessors(chain = true)
@Entity
@DiscriminatorValue("TRANSFER")
public class TransferTransaction extends Transaction {

    @OneToOne(mappedBy = "transaction", cascade = CascadeType.ALL)
    private IncomeOperation enrollmentOperation;

    @OneToOne(mappedBy = "transaction", cascade = CascadeType.ALL)
    private OutcomeOperation withdrawalOperation;

    @Override
    public TransactionDetails execute(TransactionExecutor visitor) {
        return visitor.execute(this);
    }
}
