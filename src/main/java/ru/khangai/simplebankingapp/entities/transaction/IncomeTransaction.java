package ru.khangai.simplebankingapp.entities.transaction;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.khangai.simplebankingapp.dto.transactions.TransactionDetails;
import ru.khangai.simplebankingapp.entities.operations.IncomeOperation;
import ru.khangai.simplebankingapp.services.transactions.TransactionExecutor;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.OneToOne;
import java.time.LocalDateTime;

@Getter
@Setter
@Accessors(chain = true)
@Entity
@DiscriminatorValue("INCOME")
public class IncomeTransaction extends Transaction {

    @OneToOne(mappedBy = "transaction", cascade = CascadeType.ALL)
    private IncomeOperation incomeOperation;

    @Override
    public TransactionDetails execute(TransactionExecutor visitor) {
        return visitor.execute(this);
    }

    @Override
    public IncomeTransaction setId(Long id) {
        super.setId(id);
        return this;
    }

    @Override
    public IncomeTransaction setTimestamp(LocalDateTime timestamp) {
        super.setTimestamp(timestamp);
        return this;
    }
}
