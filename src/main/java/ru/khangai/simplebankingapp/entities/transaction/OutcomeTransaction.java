package ru.khangai.simplebankingapp.entities.transaction;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.khangai.simplebankingapp.dto.transactions.TransactionDetails;
import ru.khangai.simplebankingapp.entities.operations.OutcomeOperation;
import ru.khangai.simplebankingapp.services.transactions.TransactionExecutor;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.OneToOne;

@Getter
@Setter
@Accessors(chain = true)
@Entity
@DiscriminatorValue("OUTCOME")
public class OutcomeTransaction extends Transaction {

    @OneToOne(mappedBy = "transaction", cascade = CascadeType.ALL)
    private OutcomeOperation outcomeOperation;

    @Override
    public TransactionDetails execute(TransactionExecutor visitor) {
        return visitor.execute(this);
    }
}
