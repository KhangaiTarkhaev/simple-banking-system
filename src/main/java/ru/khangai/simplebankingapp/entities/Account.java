package ru.khangai.simplebankingapp.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.khangai.simplebankingapp.entities.operations.Operation;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;

@Table(name = "accounts")
@Entity
@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
public class Account {

    @Version
    private long version;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "counterparty_id")
    private Counterparty counterparty;

    @OneToMany(mappedBy = "account", cascade = CascadeType.ALL)
    private List<Card> cards;

    @OneToMany(mappedBy = "account", cascade = CascadeType.ALL)
    private List<Operation> operations;

    @Column(name = "amount")
    private BigDecimal amount;

}
