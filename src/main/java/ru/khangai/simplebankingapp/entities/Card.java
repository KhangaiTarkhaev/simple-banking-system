package ru.khangai.simplebankingapp.entities;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.*;

@Getter
@Setter
@Accessors(chain = true)
@Table(name = "cards")
@Entity
public class Card {

    @Version
    private long version;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "account_id", nullable = false)
    private Account account;

    @Column(name = "card_number")
    private String cardNumber;

    @Column(name = "cvc_code")
    private String cvcCode;

    @Column(name = "card_holder_name")
    private String cardHolderName;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User cardHolder;

}
