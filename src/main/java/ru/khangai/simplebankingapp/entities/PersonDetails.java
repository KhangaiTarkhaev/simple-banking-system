package ru.khangai.simplebankingapp.entities;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Embeddable;

@Getter
@Setter
@Accessors(chain = true)
@Embeddable
@AttributeOverride(name = "firstName", column = @Column(name = "first_name"))
@AttributeOverride(name = "lastName", column = @Column(name = "last_name"))
@AttributeOverride(name = "email", column = @Column(name = "email"))
@AttributeOverride(name = "phoneNumber", column = @Column(name = "phone_number"))
public class PersonDetails {

    private String firstName;

    private String lastName;

    private String email;

    private String phoneNumber;

}
