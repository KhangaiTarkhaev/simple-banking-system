package ru.khangai.simplebankingapp.dto.validators;

import ru.khangai.simplebankingapp.dto.cards.CardDto;
import ru.khangai.simplebankingapp.dto.cards.CardOperationCredentials;
import ru.khangai.simplebankingapp.dto.cards.CardOperationCredentials.CardOperationType;
import ru.khangai.simplebankingapp.dto.cards.CardOperationCredentialsConstraint;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import static ru.khangai.simplebankingapp.dto.cards.CardOperationCredentials.CardOperationType.*;

public class CardOperationCredentialsValidator implements ConstraintValidator<CardOperationCredentialsConstraint, CardOperationCredentials> {

    @Override
    public boolean isValid(CardOperationCredentials cardOperationCredentials, ConstraintValidatorContext constraintValidatorContext) {
        CardDto enrollmentCard = cardOperationCredentials.getEnrollmentCard();
        CardDto withdrawalCard = cardOperationCredentials.getWithdrawalCard();
        CardOperationType type = cardOperationCredentials.getType();
        if (type == WITHDRAW && withdrawalCard == null) {
            constraintValidatorContext.buildConstraintViolationWithTemplate("when operation type is WITHDRAW withdrawal card must not be null").addConstraintViolation();
            return false;
        }
        if (type == DEPOSIT && enrollmentCard == null) {
            constraintValidatorContext.buildConstraintViolationWithTemplate("when operation type is DEPOSIT enrollment card must not be null").addConstraintViolation();
            return false;
        }
        if (type == TRANSFER && enrollmentCard == null && withdrawalCard == null) {
            constraintValidatorContext.buildConstraintViolationWithTemplate("when operation type is TRANSFER both cards must not be null").addConstraintViolation();
            return false;
        }
        return true;
    }
}
