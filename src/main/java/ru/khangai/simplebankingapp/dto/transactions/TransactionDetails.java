package ru.khangai.simplebankingapp.dto.transactions;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@Accessors(chain = true)
@JsonInclude(value = JsonInclude.Include.NON_NULL)
public class TransactionDetails {

    private Long id;

    private Long outcomeAccountId;

    private Long incomeAccountId;

    private BigDecimal amount;

    private Type type;

    private LocalDateTime timestamp;

    public enum Type {
        INCOME,
        OUTCOME,
        TRANSFER
    }

}
