package ru.khangai.simplebankingapp.dto.transactions;

public interface OutcomeTransactionCredentials extends TransactionCredentials {

    Long getWithdrawalAccountId();

}
