package ru.khangai.simplebankingapp.dto.transactions;

import lombok.Data;
import lombok.experimental.Accessors;
import ru.khangai.simplebankingapp.entities.transaction.TransferTransaction;
import ru.khangai.simplebankingapp.services.transactions.TransactionBuilder;

import java.math.BigDecimal;

@Data
@Accessors(chain = true)
public class TransferTransactionCredentials implements IncomeTransactionCredentials, OutcomeTransactionCredentials {

    private BigDecimal amount;

    private Long withdrawalAccountId;

    private Long enrollmentAccountId;

    @Override
    public TransferTransaction createTransaction(TransactionBuilder visitor) {
        return visitor.build(this);
    }

}
