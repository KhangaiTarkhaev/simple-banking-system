package ru.khangai.simplebankingapp.dto.transactions;

public interface IncomeTransactionCredentials extends TransactionCredentials {

    Long getEnrollmentAccountId();

}
