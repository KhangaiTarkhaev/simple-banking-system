package ru.khangai.simplebankingapp.dto.transactions;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import ru.khangai.simplebankingapp.entities.transaction.Transaction;
import ru.khangai.simplebankingapp.services.transactions.TransactionBuilder;

import java.math.BigDecimal;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.PROPERTY)
@JsonSubTypes({
        @Type(value = ExternalOutcomeTransactionCredentials.class, name = "externalOutcomeTransactionCredentials"),
        @Type(value = ExternalIncomeTransactionCredentials.class, name = "externalIncomeTransactionCredentials"),
        @Type(value = TransferTransactionCredentials.class, name = "transferTransactionCredentials")
})
public interface TransactionCredentials {

    BigDecimal getAmount();

    Transaction createTransaction(TransactionBuilder visitor);

}
