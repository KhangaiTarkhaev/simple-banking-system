package ru.khangai.simplebankingapp.dto.transactions;

import lombok.Data;
import lombok.experimental.Accessors;
import ru.khangai.simplebankingapp.entities.transaction.Transaction;
import ru.khangai.simplebankingapp.services.transactions.TransactionBuilder;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
public class ExternalIncomeTransactionCredentials implements IncomeTransactionCredentials {

    @Positive
    private BigDecimal amount;

    @NotNull
    private Long enrollmentAccountId;

    @Override
    public Transaction createTransaction(TransactionBuilder visitor) {
        return visitor.build(this);
    }
}
