package ru.khangai.simplebankingapp.dto;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class IdWrapper {

    private Long id;

}
