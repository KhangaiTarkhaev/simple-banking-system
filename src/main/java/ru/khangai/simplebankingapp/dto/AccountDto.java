package ru.khangai.simplebankingapp.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.experimental.Accessors;
import ru.khangai.simplebankingapp.entities.Card;
import ru.khangai.simplebankingapp.entities.Counterparty;
import ru.khangai.simplebankingapp.entities.operations.Operation;

import java.math.BigDecimal;
import java.util.List;

@Data
@Accessors(chain = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AccountDto {

    private Long id;

    private Long counterpartyId;

    private BigDecimal amount;

}
