package ru.khangai.simplebankingapp.dto.counterparty;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;

@Data
@Accessors(chain = true)
public class CounterpartyCredentials {

    @NotNull
    private Long id;

    @NotNull
    private Long counterpartyIdToBind;

}
