package ru.khangai.simplebankingapp.dto.counterparty;

import lombok.Data;
import lombok.experimental.Accessors;
import ru.khangai.simplebankingapp.dto.PersonDetailsDto;

@Data
@Accessors(chain = true)
public class UserDto extends CounterpartyDto {

    private PersonDetailsDto personDetailsDto;

    @Override
    public UserDto setId(Long id) {
        super.setId(id);
        return this;
    }


}
