package ru.khangai.simplebankingapp.dto.counterparty;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Collection;

@Data
@Accessors(chain = true)
public class MultipleCounterpartyResponse {

    private Collection<CounterpartyDto> counterparties;

}
