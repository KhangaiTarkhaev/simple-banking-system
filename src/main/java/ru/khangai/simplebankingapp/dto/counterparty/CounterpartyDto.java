package ru.khangai.simplebankingapp.dto.counterparty;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class CounterpartyDto {

    Long id;

}
