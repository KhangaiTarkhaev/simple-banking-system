package ru.khangai.simplebankingapp.dto.cards;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
@Accessors(chain = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CardDto {

    @Size(min = 16, max = 16)
    private String cardNumber;

    @Size(min = 3, max = 3)
    private String cvcCode;

    @NotBlank
    private String cardHolderName;

}
