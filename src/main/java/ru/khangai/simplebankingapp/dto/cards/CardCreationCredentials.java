package ru.khangai.simplebankingapp.dto.cards;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;

@Data
@Accessors(chain = true)
public class CardCreationCredentials {

    @NotNull
    private Long accountId;

}
