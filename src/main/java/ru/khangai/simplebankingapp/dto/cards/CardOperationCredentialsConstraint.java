package ru.khangai.simplebankingapp.dto.cards;

import ru.khangai.simplebankingapp.dto.validators.CardOperationCredentialsValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Constraint(validatedBy = CardOperationCredentialsValidator.class)
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface CardOperationCredentialsConstraint {

    String message() default "Card operation credentials is not valid";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
