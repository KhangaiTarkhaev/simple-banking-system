package ru.khangai.simplebankingapp.dto.cards;

import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

@Data
@Accessors(chain = true)
public class CardAmountDto {

    private BigDecimal amount;

}
