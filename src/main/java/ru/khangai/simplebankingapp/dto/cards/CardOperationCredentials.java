package ru.khangai.simplebankingapp.dto.cards;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
@CardOperationCredentialsConstraint
public class CardOperationCredentials {

    @NotNull
    @Positive
    private BigDecimal amount;

    private CardDto enrollmentCard;

    private CardDto withdrawalCard;

    private CardOperationType type;

    public enum CardOperationType {
        DEPOSIT,
        WITHDRAW,
        TRANSFER
    }

}
