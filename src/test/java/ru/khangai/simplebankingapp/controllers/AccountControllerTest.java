package ru.khangai.simplebankingapp.controllers;

import org.junit.jupiter.api.Test;
import org.springframework.test.context.jdbc.Sql;
import ru.khangai.simplebankingapp.dto.IdWrapper;

import java.math.BigDecimal;

import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@Sql(scripts = "/sql/account_controller.sql")
class AccountControllerTest extends AbstractControllerIT {

    @Test
    void addAccount() throws Exception {
        mockMvc.perform(post("/accounts")
                        .contentType(APPLICATION_JSON)
                        .content(om.writeValueAsString(new IdWrapper()
                                .setId(1L))))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(APPLICATION_JSON))
                .andExpect(jsonPath("$.id").value(1L))
                .andExpect(jsonPath("$.counterpartyId").value(1L))
                .andExpect(jsonPath("$.amount").value(BigDecimal.ZERO));
    }
}