package ru.khangai.simplebankingapp.controllers;

import org.junit.jupiter.api.Test;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import ru.khangai.simplebankingapp.dto.PersonDetailsDto;
import ru.khangai.simplebankingapp.dto.counterparty.UserDto;

import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class UserControllerIT extends AbstractControllerIT {

    @Test
    void createNewUser() throws Exception {
        UserDto request = new UserDto()
                .setPersonDetailsDto(new PersonDetailsDto()
                        .setFirstName("alexander")
                        .setLastName("pushkin")
                        .setEmail("alexanderpushkin@mail.ru")
                        .setPhoneNumber("+98765432"));

        UserDto expected = request.setId(1L);

        mockMvc.perform(MockMvcRequestBuilders.post("/users")
                        .contentType(APPLICATION_JSON)
                        .content(om.writeValueAsString(request)))
                .andExpect(status().isCreated())
                .andExpect(content().json(om.writeValueAsString(expected)));
    }
}