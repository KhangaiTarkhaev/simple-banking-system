package ru.khangai.simplebankingapp.controllers;

import org.junit.jupiter.api.Test;
import org.springframework.test.context.jdbc.Sql;
import ru.khangai.simplebankingapp.dto.transactions.ExternalIncomeTransactionCredentials;
import ru.khangai.simplebankingapp.dto.transactions.ExternalOutcomeTransactionCredentials;
import ru.khangai.simplebankingapp.dto.transactions.TransferTransactionCredentials;

import java.math.BigDecimal;

import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static ru.khangai.simplebankingapp.dto.transactions.TransactionDetails.Type.*;

@Sql(scripts = "/sql/transaction_controller_test.sql")
class TransactionControllerIT extends AbstractControllerIT {

    @Test
    void processTransaction_ExternalIncome() throws Exception {
        ExternalIncomeTransactionCredentials request = new ExternalIncomeTransactionCredentials()
                .setAmount(BigDecimal.TEN)
                .setEnrollmentAccountId(1L);

        mockMvc.perform(post("/transactions")
                        .contentType(APPLICATION_JSON)
                        .content(om.writeValueAsString(request)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id").value(1L))
                .andExpect(jsonPath("$.amount").value(BigDecimal.TEN))
                .andExpect(jsonPath("$.type").value(INCOME.name()))
                .andExpect(jsonPath("$.timestamp").exists())
                .andExpect(jsonPath("$.incomeAccountId").value(1L));
    }

    @Test
    void processTransaction_ExternalOutcome() throws Exception {
        ExternalOutcomeTransactionCredentials request = new ExternalOutcomeTransactionCredentials()
                .setAmount(BigDecimal.TEN)
                .setWithdrawalAccountId(1L);

        mockMvc.perform(post("/transactions")
                        .contentType(APPLICATION_JSON)
                        .content(om.writeValueAsString(request)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id").value(1L))
                .andExpect(jsonPath("$.amount").value(BigDecimal.TEN))
                .andExpect(jsonPath("$.type").value(OUTCOME.name()))
                .andExpect(jsonPath("$.timestamp").exists())
                .andExpect(jsonPath("$.outcomeAccountId").value(1L));
    }

    @Test
    void processTransaction_transfer() throws Exception {
        TransferTransactionCredentials request = new TransferTransactionCredentials()
                .setAmount(BigDecimal.TEN)
                .setEnrollmentAccountId(1L)
                .setWithdrawalAccountId(2L);

        mockMvc.perform(post("/transactions")
                        .contentType(APPLICATION_JSON)
                        .content(om.writeValueAsString(request)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id").value(1L))
                .andExpect(jsonPath("$.amount").value(BigDecimal.TEN))
                .andExpect(jsonPath("$.type").value(TRANSFER.name()))
                .andExpect(jsonPath("$.timestamp").exists())
                .andExpect(jsonPath("$.incomeAccountId").value(1L))
                .andExpect(jsonPath("$.outcomeAccountId").value(2L));
    }
}