package ru.khangai.simplebankingapp.controllers;

import org.junit.jupiter.api.Test;
import org.springframework.test.context.jdbc.Sql;
import ru.khangai.simplebankingapp.dto.PersonDetailsDto;
import ru.khangai.simplebankingapp.dto.counterparty.CounterpartyCredentials;
import ru.khangai.simplebankingapp.dto.counterparty.CounterpartyDto;
import ru.khangai.simplebankingapp.dto.counterparty.MultipleCounterpartyResponse;
import ru.khangai.simplebankingapp.dto.counterparty.UserDto;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


class CounterpartyControllerIT extends AbstractControllerIT {

    @Test
    @Sql(scripts = "/sql/counterparty_controller_test_add_new_counterparty.sql")
    void addNewCounterparty() throws Exception {
        CounterpartyCredentials request = new CounterpartyCredentials()
                .setId(1L)
                .setCounterpartyIdToBind(2L);

        CounterpartyDto counterpartyDto = new UserDto().setId(2L)
                .setPersonDetailsDto(new PersonDetailsDto()
                        .setFirstName("alexander")
                        .setLastName("pushkin")
                        .setEmail("alexanderpushkin@mail.ru")
                        .setPhoneNumber("+98765432"));

        mockMvc.perform(post("/users/counterparties")
                        .contentType(APPLICATION_JSON)
                        .content(om.writeValueAsString(request)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(APPLICATION_JSON))
                .andExpect(content().json(om.writeValueAsString(counterpartyDto)));
    }

    @Test
    @Sql(scripts = "/sql/counterparty_controller_test_get_all_counterparties.sql")
    void getAllCounterparty() throws Exception {
        CounterpartyDto counterpartyDto = new UserDto().setId(2L)
                .setPersonDetailsDto(new PersonDetailsDto()
                        .setFirstName("alexander")
                        .setLastName("pushkin")
                        .setEmail("alexanderpushkin@mail.ru")
                        .setPhoneNumber("+98765432"));

        CounterpartyDto counterpartyDto2 = new UserDto().setId(3L)
                .setPersonDetailsDto(new PersonDetailsDto()
                        .setFirstName("Geralt")
                        .setLastName("Of Rivia")
                        .setEmail("Geralt@mail.ru")
                        .setPhoneNumber("+12345678"));

        List<CounterpartyDto> counterpartyDtos = new ArrayList<>();
        counterpartyDtos.add(counterpartyDto);
        counterpartyDtos.add(counterpartyDto2);

        MultipleCounterpartyResponse expected = new MultipleCounterpartyResponse().setCounterparties(counterpartyDtos);

        mockMvc.perform(get("/users/counterparties")
                        .param("counterpartyId", "1"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(APPLICATION_JSON))
                .andExpect(content().json(om.writeValueAsString(expected)));
    }
}