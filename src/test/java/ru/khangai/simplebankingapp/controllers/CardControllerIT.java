package ru.khangai.simplebankingapp.controllers;

import org.junit.jupiter.api.Test;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MvcResult;
import ru.khangai.simplebankingapp.dto.cards.CardAmountDto;
import ru.khangai.simplebankingapp.dto.cards.CardCreationCredentials;
import ru.khangai.simplebankingapp.dto.cards.CardDto;
import ru.khangai.simplebankingapp.dto.cards.CardOperationCredentials;
import ru.khangai.simplebankingapp.dto.transactions.TransactionDetails;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static ru.khangai.simplebankingapp.dto.cards.CardOperationCredentials.CardOperationType.*;
import static ru.khangai.simplebankingapp.dto.transactions.TransactionDetails.Type.INCOME;
import static ru.khangai.simplebankingapp.dto.transactions.TransactionDetails.Type.OUTCOME;


@Sql(scripts = "/sql/card_controller_test.sql")
class CardControllerIT extends AbstractControllerIT {


    @Test
    void createNewCard() throws Exception {
        MvcResult mvcResult = mockMvc.perform(post("/cards")
                        .contentType(APPLICATION_JSON)
                        .content(om.writeValueAsString(new CardCreationCredentials().setAccountId(1L))))
                .andExpect(status().isCreated())
                .andDo(print())
                .andExpect(content().contentType(APPLICATION_JSON))
                .andReturn();

        String jsonResponse = mvcResult.getResponse().getContentAsString();
        CardDto actual = om.readValue(jsonResponse, CardDto.class);

        assertNotNull(actual);
        assertTrue(actual.getCardNumber().matches("[0-9]{16}"));
        assertTrue(actual.getCvcCode().matches("[0-9]{3}"));
        assertEquals("KHANGAI TARKHAEV", actual.getCardHolderName());
    }

    @Test
    void getCardList() throws Exception {
        CardDto cardDto = new CardDto()
                .setCardNumber("1234567891234567")
                .setCardHolderName("KHANGAI TARKHAEV")
                .setCvcCode("123");

        CardDto cardDto2 = new CardDto()
                .setCardNumber("9876543219876543")
                .setCardHolderName("KHANGAI TARKHAEV")
                .setCvcCode("321");

        List<CardDto> expected = new ArrayList<>();
        expected.add(cardDto);
        expected.add(cardDto2);

        mockMvc.perform(get("/cards")
                        .param("counterpartyId", "1"))
                .andExpect(content().contentType(APPLICATION_JSON))
                .andExpect(content().json(om.writeValueAsString(expected)));
    }

    @Test
    void checkBalance() throws Exception {
        CardDto request = new CardDto()
                .setCardNumber("1234567891234567")
                .setCardHolderName("KHANGAI TARKHAEV")
                .setCvcCode("123");

        CardAmountDto expected = new CardAmountDto().setAmount(BigDecimal.TEN);

        mockMvc.perform(post("/cards/balance")
                        .contentType(APPLICATION_JSON)
                        .content(om.writeValueAsString(request)))
                .andExpect(content()
                        .json(om.writeValueAsString(expected)));
    }

    @Test
    void executeCardOperation_deposit() throws Exception {
        CardOperationCredentials cardOperationCredentials = new CardOperationCredentials()
                .setEnrollmentCard(new CardDto()
                        .setCardNumber("1234567891234567")
                        .setCardHolderName("KHANGAI TARKHAEV")
                        .setCvcCode("123"))
                .setType(DEPOSIT)
                .setAmount(BigDecimal.TEN);

        mockMvc.perform(post("/cards/transactions")
                        .contentType(APPLICATION_JSON)
                        .content(om.writeValueAsString(cardOperationCredentials)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(1L))
                .andExpect(jsonPath("$.amount").value(BigDecimal.TEN))
                .andExpect(jsonPath("$.type").value(INCOME.name()))
                .andExpect(jsonPath("$.timestamp").exists())
                .andExpect(jsonPath("$.incomeAccountId").value(1L));
    }

    @Test
    void executeCardOperation_withdraw() throws Exception {
        CardOperationCredentials cardOperationCredentials = new CardOperationCredentials()
                .setWithdrawalCard(new CardDto()
                        .setCardNumber("1234567891234567")
                        .setCardHolderName("KHANGAI TARKHAEV")
                        .setCvcCode("123"))
                .setType(WITHDRAW)
                .setAmount(BigDecimal.TEN);

        mockMvc.perform(post("/cards/transactions")
                        .contentType(APPLICATION_JSON)
                        .content(om.writeValueAsString(cardOperationCredentials)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(1L))
                .andExpect(jsonPath("$.amount").value(BigDecimal.TEN))
                .andExpect(jsonPath("$.type").value(OUTCOME.name()))
                .andExpect(jsonPath("$.timestamp").exists())
                .andExpect(jsonPath("$.outcomeAccountId").value(1L));
    }

    @Test
    void executeCardOperation_transfer() throws Exception {
        CardOperationCredentials cardOperationCredentials = new CardOperationCredentials()
                .setEnrollmentCard(new CardDto()
                        .setCardNumber("1234567891234567")
                        .setCardHolderName("KHANGAI TARKHAEV")
                        .setCvcCode("123"))
                .setWithdrawalCard(new CardDto()
                        .setCardNumber("9876543219876543")
                        .setCardHolderName("KHANGAI TARKHAEV")
                        .setCvcCode("321"))
                .setType(TRANSFER)
                .setAmount(BigDecimal.TEN);

        mockMvc.perform(post("/cards/transactions")
                        .contentType(APPLICATION_JSON)
                        .content(om.writeValueAsString(cardOperationCredentials)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(1L))
                .andExpect(jsonPath("$.amount").value(BigDecimal.TEN))
                .andExpect(jsonPath("$.type").value(TransactionDetails.Type.TRANSFER.name()))
                .andExpect(jsonPath("$.timestamp").exists())
                .andExpect(jsonPath("$.incomeAccountId").value(1L))
                .andExpect(jsonPath("$.outcomeAccountId").value(2L));
    }


}