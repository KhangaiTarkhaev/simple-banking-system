package ru.khangai.simplebankingapp.converters;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import ru.khangai.simplebankingapp.dto.cards.CardDto;
import ru.khangai.simplebankingapp.entities.Account;
import ru.khangai.simplebankingapp.entities.Card;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class CardToDtoConverterTest {

    CardToDtoConverter subject;

    @BeforeEach
    void setUp() {
        subject = new CardToDtoConverter();
    }

    @AfterEach
    void tearDown() {
        subject = null;
    }

    @Test
    void convert() {
        Card card = new Card()
                .setId(1L)
                .setCardNumber("9876543219876543")
                .setCardHolderName("KHANGAI TARKHAEV")
                .setCvcCode("321")
                .setAccount(new Account().setId(1L));

        CardDto actual = subject.convert(card);

        assertNotNull(actual);
        assertEquals("9876543219876543", actual.getCardNumber());
        assertEquals("KHANGAI TARKHAEV", actual.getCardHolderName());
        assertEquals("321", actual.getCvcCode());
    }
}