package ru.khangai.simplebankingapp.converters;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import ru.khangai.simplebankingapp.dto.PersonDetailsDto;
import ru.khangai.simplebankingapp.dto.counterparty.UserDto;
import ru.khangai.simplebankingapp.entities.PersonDetails;
import ru.khangai.simplebankingapp.entities.User;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@ExtendWith(MockitoExtension.class)
class UserToDtoConverterTest {

    @InjectMocks
    UserToDtoConverter subject;

    @Spy
    PersonDetailsToDtoConverter personDetailsToDtoConverter = new PersonDetailsToDtoConverter();

    @Test
    void convert() {
        User user = new User();
        user.setId(1L);
        user.setDetails(new PersonDetails()
                .setFirstName("alexander")
                .setLastName("pushkin")
                .setEmail("alexanderpushkin@mail.ru")
                .setPhoneNumber("+98765432"));

        UserDto actual = subject.convert(user);

        assertNotNull(actual);
        assertEquals(1L, actual.getId());
        PersonDetailsDto personDetailsDto = actual.getPersonDetailsDto();
        assertEquals("alexander", personDetailsDto.getFirstName());
        assertEquals("pushkin", personDetailsDto.getLastName());
        assertEquals("alexanderpushkin@mail.ru", personDetailsDto.getEmail());
        assertEquals("+98765432", personDetailsDto.getPhoneNumber());
    }
}