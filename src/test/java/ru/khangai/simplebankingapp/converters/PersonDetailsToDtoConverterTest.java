package ru.khangai.simplebankingapp.converters;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import ru.khangai.simplebankingapp.dto.PersonDetailsDto;
import ru.khangai.simplebankingapp.entities.PersonDetails;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class PersonDetailsToDtoConverterTest {

    PersonDetailsToDtoConverter subject;

    @BeforeEach
    void setUp() {
        subject = new PersonDetailsToDtoConverter();
    }

    @Test
    void convert() {
        PersonDetails personDetails = new PersonDetails()
                .setFirstName("alexander")
                .setLastName("pushkin")
                .setEmail("alexanderpushkin@mail.ru")
                .setPhoneNumber("+98765432");

        PersonDetailsDto actual = subject.convert(personDetails);

        assertNotNull(actual);
        assertEquals("alexanderpushkin@mail.ru", actual.getEmail());
        assertEquals("alexander", actual.getFirstName());
        assertEquals("pushkin", actual.getLastName());
        assertEquals("+98765432", actual.getPhoneNumber());
    }
}