package ru.khangai.simplebankingapp.converters;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import ru.khangai.simplebankingapp.dto.transactions.TransactionDetails;
import ru.khangai.simplebankingapp.entities.Account;
import ru.khangai.simplebankingapp.entities.operations.IncomeOperation;
import ru.khangai.simplebankingapp.entities.operations.OutcomeOperation;
import ru.khangai.simplebankingapp.entities.transaction.TransferTransaction;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class TransferTransactionToDetailsConverterTest {

    TransferTransactionToDetailsConverter subject;

    @BeforeEach
    void setUp() {
        subject = new TransferTransactionToDetailsConverter();
    }

    @Test
    void convert() {
        TransferTransaction transferTransaction = new TransferTransaction();
        transferTransaction.setId(1L);
        transferTransaction.setTimestamp(LocalDateTime.MAX);

        OutcomeOperation outcomeOperation = new OutcomeOperation()
                .setTransaction(transferTransaction)
                .setAccount(new Account()
                        .setId(1L))
                .setAmount(BigDecimal.TEN);

        IncomeOperation incomeOperation = new IncomeOperation()
                .setTransaction(transferTransaction)
                .setAccount(new Account()
                        .setId(2L))
                .setAmount(BigDecimal.TEN);

        transferTransaction.setWithdrawalOperation(outcomeOperation);
        transferTransaction.setEnrollmentOperation(incomeOperation);

        TransactionDetails actual = subject.convert(transferTransaction);

        assertNotNull(actual);
        assertEquals(1L, actual.getId());
        assertEquals(1L, actual.getOutcomeAccountId());
        assertEquals(2L, actual.getIncomeAccountId());
        assertEquals(BigDecimal.TEN, actual.getAmount());
        assertEquals(LocalDateTime.MAX, actual.getTimestamp());
        assertEquals(TransactionDetails.Type.TRANSFER, actual.getType());
    }
}