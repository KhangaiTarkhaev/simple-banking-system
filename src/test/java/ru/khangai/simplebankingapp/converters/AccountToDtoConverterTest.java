package ru.khangai.simplebankingapp.converters;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import ru.khangai.simplebankingapp.dto.AccountDto;
import ru.khangai.simplebankingapp.entities.Account;
import ru.khangai.simplebankingapp.entities.User;

import java.math.BigDecimal;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.comparesEqualTo;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class AccountToDtoConverterTest {

    AccountToDtoConverter subject;

    @BeforeEach
    void setUp() {
        subject = new AccountToDtoConverter();
    }

    @Test
    void convert() {
        User user = new User();
        user.setId(1L);

        Account accountToConvert = new Account().setId(1L)
                .setAmount(BigDecimal.TEN)
                .setCounterparty(user);

        AccountDto actual = subject.convert(accountToConvert);

        assertNotNull(actual);
        assertThat(BigDecimal.TEN, comparesEqualTo(actual.getAmount()));
        assertEquals(1L, actual.getId());
        assertEquals(1L, actual.getCounterpartyId());
    }
}