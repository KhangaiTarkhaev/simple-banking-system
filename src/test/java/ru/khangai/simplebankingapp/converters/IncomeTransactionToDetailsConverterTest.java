package ru.khangai.simplebankingapp.converters;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import ru.khangai.simplebankingapp.dto.transactions.TransactionDetails;
import ru.khangai.simplebankingapp.entities.Account;
import ru.khangai.simplebankingapp.entities.operations.IncomeOperation;
import ru.khangai.simplebankingapp.entities.transaction.IncomeTransaction;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class IncomeTransactionToDetailsConverterTest {

    IncomeTransactionToDetailsConverter subject;

    @BeforeEach
    void setUp() {
        subject = new IncomeTransactionToDetailsConverter();
    }

    @AfterEach
    void tearDown() {
        subject = null;
    }

    @Test
    void convert() {
        IncomeTransaction incomeTransaction = new IncomeTransaction();
        incomeTransaction.setId(1L);
        incomeTransaction.setTimestamp(LocalDateTime.MAX);
        IncomeOperation incomeOperation = new IncomeOperation()
                .setTransaction(incomeTransaction)
                .setAccount(new Account()
                        .setId(1L))
                .setAmount(BigDecimal.TEN);
        incomeTransaction.setIncomeOperation(incomeOperation);

        TransactionDetails actual = subject.convert(incomeTransaction);

        assertNotNull(actual);
        assertEquals(1L, actual.getId());
        assertEquals(1L, actual.getIncomeAccountId());
        assertEquals(BigDecimal.TEN, actual.getAmount());
        assertEquals(LocalDateTime.MAX, actual.getTimestamp());
        assertEquals(TransactionDetails.Type.INCOME, actual.getType());
    }
}