package ru.khangai.simplebankingapp.converters;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import ru.khangai.simplebankingapp.dto.transactions.TransactionDetails;
import ru.khangai.simplebankingapp.entities.Account;
import ru.khangai.simplebankingapp.entities.operations.OutcomeOperation;
import ru.khangai.simplebankingapp.entities.transaction.OutcomeTransaction;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class OutcomeTransactionToDetailsConverterTest {

    OutcomeTransactionToDetailsConverter subject;

    @BeforeEach
    void setUp() {
        subject = new OutcomeTransactionToDetailsConverter();
    }

    @AfterEach
    void tearDown() {
        subject = null;
    }

    @Test
    void convert() {
        OutcomeTransaction outcomeTransaction = new OutcomeTransaction();
        outcomeTransaction.setId(1L);
        outcomeTransaction.setTimestamp(LocalDateTime.MAX);
        OutcomeOperation outcomeOperation = new OutcomeOperation()
                .setTransaction(outcomeTransaction)
                .setAccount(new Account()
                        .setId(1L))
                .setAmount(BigDecimal.TEN);
        outcomeTransaction.setOutcomeOperation(outcomeOperation);

        TransactionDetails actual = subject.convert(outcomeTransaction);

        assertNotNull(actual);
        assertEquals(1L, actual.getId());
        assertEquals(1L, actual.getOutcomeAccountId());
        assertEquals(BigDecimal.TEN, actual.getAmount());
        assertEquals(LocalDateTime.MAX, actual.getTimestamp());
        assertEquals(TransactionDetails.Type.OUTCOME, actual.getType());
    }
}