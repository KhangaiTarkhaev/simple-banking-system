package ru.khangai.simplebankingapp.dao;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.jdbc.Sql;
import ru.khangai.simplebankingapp.entities.Counterparty;
import ru.khangai.simplebankingapp.entities.PersonDetails;
import ru.khangai.simplebankingapp.entities.User;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@Sql(scripts = "/sql/counterparty_dao_test.sql")
@Import(CounterpartyDaoImpl.class)
class CounterpartyDaoImplIT extends AbstractDaoIT {

    @Autowired
    CounterpartyDaoImpl subject;

    @Test
    void bindCounterparties() {
        Counterparty counterparty = subject.bindCounterparties(1L, 3L);

        User actual = (User) counterparty;
        assertEquals(3L, actual.getId());

        PersonDetails details = actual.getDetails();
        assertEquals("Geralt", details.getFirstName());
        assertEquals("Of Rivia", details.getLastName());
        assertEquals("+1010101010", details.getPhoneNumber());
        assertEquals("geraltofrivia@mail.ru", details.getEmail());
    }

    @Test
    void getAllCounterpartyOf() {
        List<Counterparty> counterparties = subject.getAllCounterpartyOf(1L);

        assertEquals(1, counterparties.size());
        Counterparty counterparty = counterparties.get(0);

        assertEquals(2L, counterparty.getId());

        List<Counterparty> reverse = subject.getAllCounterpartyOf(2L);

        assertEquals(1, reverse.size());
        Counterparty reverseCounterparty = reverse.get(0);

        assertEquals(1L, reverseCounterparty.getId());
    }

    @Test
    void findById() {
        User actual = (User) subject.findById(1L);

        assertEquals(1L, actual.getId());

        PersonDetails details = actual.getDetails();
        assertEquals("khangai", details.getFirstName());
        assertEquals("tarkhaev", details.getLastName());
        assertEquals("+12345678", details.getPhoneNumber());
        assertEquals("khangai@mail.ru", details.getEmail());
    }
}