package ru.khangai.simplebankingapp.dao;

import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.jdbc.Sql;
import ru.khangai.simplebankingapp.entities.Account;
import ru.khangai.simplebankingapp.entities.Card;
import ru.khangai.simplebankingapp.entities.User;

import java.math.BigDecimal;
import java.util.Collection;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

@Sql(scripts = "/sql/dao_test.sql")
@Import(CardDaoImpl.class)
class CardDaoImplIT extends AbstractDaoIT {

    @Autowired
    CardDaoImpl subject;

    @Test
    void getAllCardsByUserId() {
        Collection<Card> actual = subject.getAllCardsByUserId(1L);

        User user = new User();
        user.setId(1L);

        assertEquals(1, actual.size());
        Card card = (Card) actual.toArray()[0];
        assertEquals(1L, card.getId());
        assertEquals(1L, card.getAccount().getId());
        assertEquals("KHANGAI TARKHAEV", card.getCardHolderName());
        assertEquals(1L, card.getCardHolder().getId());
        assertEquals("123", card.getCvcCode());
        assertEquals("1234567891234567", card.getCardNumber());
    }

    @Test
    void save() {
        User user = new User();
        user.setId(1L);
        Card cardToSave = new Card()
                .setAccount(new Account()
                        .setId(1L))
                .setCardHolderName("KHANGAI TARKHAEV")
                .setCardHolder(user)
                .setCardNumber("1234567890")
                .setCvcCode("987");

        Card actual = subject.save(cardToSave);

        assertEquals(3L, actual.getId());
    }

    @Test
    void getCardAmountById() {
        BigDecimal actual = subject.getCardAmountById(1L);

        assertThat(BigDecimal.TEN, Matchers.comparesEqualTo(actual));
    }

    @Test
    void findByCardNumber() {
        Card actual = subject.findByCardNumber("1234567891234567");

        assertEquals(1L, actual.getId());
        assertEquals(1L, actual.getAccount().getId());
        assertEquals("KHANGAI TARKHAEV", actual.getCardHolderName());
        assertEquals(1L, actual.getCardHolder().getId());
        assertEquals("123", actual.getCvcCode());
        assertEquals("1234567891234567", actual.getCardNumber());
    }
}