package ru.khangai.simplebankingapp.dao;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.jdbc.Sql;
import ru.khangai.simplebankingapp.entities.PersonDetails;
import ru.khangai.simplebankingapp.entities.User;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@Sql(scripts = "/sql/dao_test.sql")
@Import(UserDaoImpl.class)
class UserDaoImplIT extends AbstractDaoIT {

    @Autowired
    UserDaoImpl subject;


    @Test
    void findById() {
        User actual = subject.findById(1L);

        assertEquals(1L, actual.getId());

        PersonDetails details = actual.getDetails();
        assertEquals("khangai", details.getFirstName());
        assertEquals("tarkhaev", details.getLastName());
        assertEquals("+12345678", details.getPhoneNumber());
        assertEquals("khangai@mail.ru", details.getEmail());
    }

    @Test
    void save() {
        PersonDetails personDetails = new PersonDetails()
                .setFirstName("newFirstName")
                .setLastName("newLastName")
                .setEmail("newEmail")
                .setPhoneNumber("someNumber");

        User user = new User().
                setDetails(personDetails);

        User actual = subject.save(user);

        assertNotNull(actual);
        assertEquals(3L, actual.getId());
    }

}