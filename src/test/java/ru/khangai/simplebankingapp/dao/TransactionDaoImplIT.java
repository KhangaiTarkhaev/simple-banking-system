package ru.khangai.simplebankingapp.dao;

import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.transaction.annotation.Transactional;
import ru.khangai.simplebankingapp.entities.Account;
import ru.khangai.simplebankingapp.entities.operations.IncomeOperation;
import ru.khangai.simplebankingapp.entities.operations.OutcomeOperation;
import ru.khangai.simplebankingapp.entities.transaction.IncomeTransaction;
import ru.khangai.simplebankingapp.entities.transaction.OutcomeTransaction;
import ru.khangai.simplebankingapp.entities.transaction.TransferTransaction;

import javax.persistence.EntityManager;
import java.math.BigDecimal;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@Sql("/sql/transaction_dao_test.sql")
@Import(TransactionDaoImpl.class)
@Transactional
class TransactionDaoImplIT extends AbstractDaoIT {

    @Autowired
    TransactionDaoImpl subject;

    @Autowired
    EntityManager em;

    @Test
    void save_transfer() {
        TransferTransaction transferTransaction = new TransferTransaction();

        OutcomeOperation outcomeOperation = new OutcomeOperation()
                .setTransaction(transferTransaction)
                .setAccount(new Account()
                        .setId(1L))
                .setAmount(BigDecimal.TEN);

        IncomeOperation incomeOperation = new IncomeOperation()
                .setTransaction(transferTransaction)
                .setAccount(new Account()
                        .setId(2L))
                .setAmount(BigDecimal.TEN);

        transferTransaction.setWithdrawalOperation(outcomeOperation);
        transferTransaction.setEnrollmentOperation(incomeOperation);

        TransferTransaction actual = subject.save(transferTransaction);

        assertEquals(1L, actual.getId());
        assertNotNull(actual.getTimestamp());

        IncomeOperation savedIncomeOp = em.createQuery("SELECT o FROM  IncomeOperation o WHERE o.transaction.id=:transId", IncomeOperation.class)
                .setParameter("transId", 1L).getSingleResult();
        assertNotNull(savedIncomeOp);
        assertNotNull(savedIncomeOp.getId());
        assertThat(BigDecimal.TEN, Matchers.comparesEqualTo(savedIncomeOp.getAmount()));
        assertEquals(1L, savedIncomeOp.getTransaction().getId());
        assertEquals(2L, savedIncomeOp.getAccount().getId());

        OutcomeOperation savedOutcomeOp = em.createQuery("SELECT o FROM OutcomeOperation o WHERE o.transaction.id=:transId", OutcomeOperation.class)
                .setParameter("transId", 1L).getSingleResult();
        assertNotNull(savedOutcomeOp);
        assertNotNull(savedOutcomeOp.getId());
        assertThat(BigDecimal.TEN, Matchers.comparesEqualTo(savedOutcomeOp.getAmount()));
        assertEquals(1L, savedOutcomeOp.getTransaction().getId());
        assertEquals(1L, savedOutcomeOp.getAccount().getId());
    }

    @Test
    void save_income() {
        IncomeTransaction incomeTransaction = new IncomeTransaction();

        IncomeOperation incomeOperation = new IncomeOperation()
                .setTransaction(incomeTransaction)
                .setAccount(new Account()
                        .setId(2L))
                .setAmount(BigDecimal.TEN);

        incomeTransaction.setIncomeOperation(incomeOperation);

        IncomeTransaction actual = subject.save(incomeTransaction);

        assertEquals(1L, actual.getId());
        assertNotNull(actual.getTimestamp());

        IncomeOperation savedIncomeOp = em.createQuery("SELECT o FROM  IncomeOperation o WHERE o.transaction.id=:transId", IncomeOperation.class)
                .setParameter("transId", 1L).getSingleResult();
        assertNotNull(savedIncomeOp);
        assertNotNull(savedIncomeOp.getId());
        assertThat(BigDecimal.TEN, Matchers.comparesEqualTo(savedIncomeOp.getAmount()));
        assertEquals(1L, savedIncomeOp.getTransaction().getId());
        assertEquals(2L, savedIncomeOp.getAccount().getId());
    }

    @Test
    void save_outcome() {
        OutcomeTransaction outcomeTransaction = new OutcomeTransaction();

        OutcomeOperation outcomeOperation = new OutcomeOperation()
                .setTransaction(outcomeTransaction)
                .setAccount(new Account()
                        .setId(1L))
                .setAmount(BigDecimal.TEN);

        outcomeTransaction.setOutcomeOperation(outcomeOperation);

        OutcomeTransaction actual = subject.save(outcomeTransaction);

        assertEquals(1L, actual.getId());
        assertNotNull(actual.getTimestamp());

        OutcomeOperation savedOutcomeOp = em.createQuery("SELECT o FROM OutcomeOperation o WHERE o.transaction.id=:transId", OutcomeOperation.class)
                .setParameter("transId", 1L).getSingleResult();
        assertNotNull(savedOutcomeOp);
        assertNotNull(savedOutcomeOp.getId());
        assertThat(BigDecimal.TEN, Matchers.comparesEqualTo(savedOutcomeOp.getAmount()));
        assertEquals(1L, savedOutcomeOp.getTransaction().getId());
        assertEquals(1L, savedOutcomeOp.getAccount().getId());
    }

}