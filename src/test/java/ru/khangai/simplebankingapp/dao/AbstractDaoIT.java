package ru.khangai.simplebankingapp.dao;

import liquibase.exception.LiquibaseException;
import liquibase.integration.spring.SpringLiquibase;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@DataJpaTest
public abstract class AbstractDaoIT {

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    SpringLiquibase liquibase;

    @AfterEach
    void tearDown() throws LiquibaseException {
        jdbcTemplate.execute("DROP ALL OBJECTS");
        liquibase.setDropFirst(true);
        liquibase.afterPropertiesSet();
    }
}
