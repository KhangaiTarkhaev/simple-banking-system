package ru.khangai.simplebankingapp.dao;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.jdbc.Sql;
import ru.khangai.simplebankingapp.entities.Account;
import ru.khangai.simplebankingapp.entities.User;

import java.math.BigDecimal;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.comparesEqualTo;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@Sql(scripts = "/sql/dao_test.sql")
@Import(AccountDaoImpl.class)
class AccountDaoImplIT extends AbstractDaoIT {

    @Autowired
    AccountDaoImpl subject;

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void findById() {
        Account actual = subject.findById(1L);

        assertEquals(1L, actual.getId());
        assertThat(BigDecimal.TEN, comparesEqualTo(actual.getAmount()));
        assertEquals(1L, actual.getCounterparty().getId());
        assertEquals(1, actual.getCards().size());
        assertEquals(1L, actual.getCards().get(0).getId());
        assertEquals(1L, actual.getCounterparty().getId());
        assertEquals(1, actual.getOperations().size());
        assertEquals(1, actual.getOperations().get(0).getId());
    }

    @Test
    void save() {
        User user = new User();
        user.setId(1L);

        Account account = new Account()
                .setAmount(BigDecimal.valueOf(30))
                .setCounterparty(user);

        Account actual = subject.save(account);

        assertNotNull(actual);
        assertEquals(3L, actual.getId());

        Account saved = subject.findById(3L);

        String sql = "SELECT counterparty_id FROM accounts WHERE id=?";

        Long counterpartyId = jdbcTemplate.queryForObject(
                sql, Long.class, 1);

        assertEquals(1L, saved.getCounterparty().getId());
    }

    @Test
    void income() {
        Account account = subject.income(1L, BigDecimal.TEN);

        assertNotNull(account);
        assertEquals(1L, account.getId());

        String sql = "SELECT amount FROM accounts WHERE id=?";
        BigDecimal amount = jdbcTemplate.queryForObject(
                sql, BigDecimal.class, 1);

        assertThat(BigDecimal.valueOf(20), comparesEqualTo(amount));
    }

    @Test
    void outcome() {
        Account account = subject.outcome(2L, BigDecimal.TEN);

        assertNotNull(account);
        assertEquals(2L, account.getId());

        String sql = "SELECT amount FROM accounts WHERE id=?";
        BigDecimal amount = jdbcTemplate.queryForObject(
                sql, BigDecimal.class, 2);

        assertThat(BigDecimal.valueOf(10), comparesEqualTo(amount));
    }
}