package ru.khangai.simplebankingapp.services.transactions;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import ru.khangai.simplebankingapp.dto.transactions.ExternalIncomeTransactionCredentials;
import ru.khangai.simplebankingapp.dto.transactions.ExternalOutcomeTransactionCredentials;
import ru.khangai.simplebankingapp.dto.transactions.TransferTransactionCredentials;
import ru.khangai.simplebankingapp.entities.operations.IncomeOperation;
import ru.khangai.simplebankingapp.entities.operations.OutcomeOperation;
import ru.khangai.simplebankingapp.entities.transaction.IncomeTransaction;
import ru.khangai.simplebankingapp.entities.transaction.OutcomeTransaction;
import ru.khangai.simplebankingapp.entities.transaction.TransferTransaction;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class TransactionBuilderImplTest {

    TransactionBuilderImpl subject;

    @BeforeEach
    void setUp() {
        subject = new TransactionBuilderImpl();

    }

    @AfterEach
    void tearDown() {
        subject = null;
    }

    @Test
    void build_externalIncomeTransactionCredentials_ok() {
        ExternalIncomeTransactionCredentials transactionCredentials = new ExternalIncomeTransactionCredentials()
                .setAmount(BigDecimal.TEN)
                .setEnrollmentAccountId(1L);

        IncomeTransaction transaction = subject.build(transactionCredentials);

        assertNotNull(transaction);

        IncomeOperation incomeOperation = transaction.getIncomeOperation();

        assertNotNull(incomeOperation);
        assertEquals(BigDecimal.TEN, incomeOperation.getAmount());
        assertEquals(1L, incomeOperation.getAccount().getId());
    }

    @Test
    void build_externalOutcomeTransactionCredentials_ok() {
        ExternalOutcomeTransactionCredentials transactionCredentials = new ExternalOutcomeTransactionCredentials()
                .setAmount(BigDecimal.TEN)
                .setWithdrawalAccountId(1L);

        OutcomeTransaction transaction = subject.build(transactionCredentials);

        assertNotNull(transaction);

        OutcomeOperation outcomeOperation = transaction.getOutcomeOperation();

        assertNotNull(outcomeOperation);
        assertEquals(BigDecimal.TEN, outcomeOperation.getAmount());
        assertEquals(1L, outcomeOperation.getAccount().getId());
    }

    @Test
    void build_transferTransactionCredentials_ok() {
        TransferTransactionCredentials transactionCredentials = new TransferTransactionCredentials()
                .setAmount(BigDecimal.TEN)
                .setWithdrawalAccountId(2L)
                .setEnrollmentAccountId(1L);

        TransferTransaction transferTransaction = subject.build(transactionCredentials);

        assertNotNull(transferTransaction);

        OutcomeOperation outcomeOperation = transferTransaction.getWithdrawalOperation();

        assertNotNull(outcomeOperation);
        assertEquals(BigDecimal.TEN, outcomeOperation.getAmount());
        assertEquals(2L, outcomeOperation.getAccount().getId());

        IncomeOperation incomeOperation = transferTransaction.getEnrollmentOperation();

        assertNotNull(incomeOperation);
        assertEquals(BigDecimal.TEN, incomeOperation.getAmount());
        assertEquals(1L, incomeOperation.getAccount().getId());
    }
}