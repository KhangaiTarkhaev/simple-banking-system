package ru.khangai.simplebankingapp.services.transactions;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.core.convert.converter.Converter;
import ru.khangai.simplebankingapp.converters.IncomeTransactionToDetailsConverter;
import ru.khangai.simplebankingapp.converters.OutcomeTransactionToDetailsConverter;
import ru.khangai.simplebankingapp.converters.TransferTransactionToDetailsConverter;
import ru.khangai.simplebankingapp.dao.AccountDao;
import ru.khangai.simplebankingapp.dao.TransactionDao;
import ru.khangai.simplebankingapp.dto.transactions.TransactionDetails;
import ru.khangai.simplebankingapp.entities.Account;
import ru.khangai.simplebankingapp.entities.operations.IncomeOperation;
import ru.khangai.simplebankingapp.entities.operations.OutcomeOperation;
import ru.khangai.simplebankingapp.entities.transaction.IncomeTransaction;
import ru.khangai.simplebankingapp.entities.transaction.OutcomeTransaction;
import ru.khangai.simplebankingapp.entities.transaction.Transaction;
import ru.khangai.simplebankingapp.entities.transaction.TransferTransaction;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.*;
import static ru.khangai.simplebankingapp.dto.transactions.TransactionDetails.Type.*;

class TransactionExecutorImplTest {

    TransactionExecutorImpl subject;


    TransactionDao transactionDao;

    AccountDao accountDao;

    IncomeTransactionToDetailsConverter incomeTransactionTransactionDetailsConverter;

    Converter<OutcomeTransaction, TransactionDetails> outcomeTransactionTransactionDetailsConverter;

    Converter<TransferTransaction, TransactionDetails> transferTransactionTransactionDetailsConverter;

    @BeforeEach
    void setUp() {
        transactionDao = mock(TransactionDao.class);
        accountDao = mock(AccountDao.class);
        incomeTransactionTransactionDetailsConverter = mock(IncomeTransactionToDetailsConverter.class);
        outcomeTransactionTransactionDetailsConverter = mock(OutcomeTransactionToDetailsConverter.class);
        transferTransactionTransactionDetailsConverter = mock(TransferTransactionToDetailsConverter.class);

        subject = new TransactionExecutorImpl(transactionDao,
                accountDao,
                incomeTransactionTransactionDetailsConverter,
                outcomeTransactionTransactionDetailsConverter,
                transferTransactionTransactionDetailsConverter);
    }

    @AfterEach
    void tearDown() {
        subject = null;
    }


    @Test
    void execute_IncomeTransaction() {
        Long id = 1L;
        mockDbGeneratedValue(id);
        mockConversionIncomeTransactionToDto();

        IncomeTransaction incomeTransaction = new IncomeTransaction();
        incomeTransaction.setIncomeOperation(new IncomeOperation()
                .setTransaction(incomeTransaction)
                .setAmount(BigDecimal.TEN)
                .setAccount(new Account().setId(1L)
                        .setAmount(BigDecimal.ZERO)));

        TransactionDetails actual = subject.execute(incomeTransaction);

        assertNotNull(actual);
        assertEquals(1L, actual.getId());
        assertEquals(BigDecimal.TEN, actual.getAmount());
        assertEquals(LocalDateTime.MAX, actual.getTimestamp());
        assertEquals(INCOME, actual.getType());
        assertEquals(1L, actual.getIncomeAccountId());

        verify(accountDao, times(1)).income(1L, BigDecimal.TEN);
    }

    private void mockConversionIncomeTransactionToDto() {
        when(incomeTransactionTransactionDetailsConverter.convert(any(IncomeTransaction.class)))
                .thenAnswer(invocationOnMock -> {
                    IncomeTransaction argument = invocationOnMock.getArgument(0, IncomeTransaction.class);
                    IncomeOperation incomeOperation = argument.getIncomeOperation();
                    return new TransactionDetails()
                            .setId(argument.getId())
                            .setAmount(incomeOperation.getAmount())
                            .setType(INCOME)
                            .setIncomeAccountId(incomeOperation.getAccount().getId())
                            .setTimestamp(argument.getTimestamp());
                });
    }

    private void mockDbGeneratedValue(Long id) {
        when(transactionDao.save(any(Transaction.class))).thenAnswer(invocationOnMock -> {
            Transaction argument = invocationOnMock.getArgument(0, Transaction.class);
            argument.setId(id);
            argument.setTimestamp(LocalDateTime.MAX);
            return argument;
        });
    }

    @Test
    void execute_transferTransaction() {
        TransferTransaction transferTransaction = new TransferTransaction();
        mockDbGeneratedValue(1L);
        when(transferTransactionTransactionDetailsConverter.convert(any(TransferTransaction.class)))
                .thenAnswer(invocationOnMock -> {
                    TransferTransaction argument = invocationOnMock.getArgument(0, TransferTransaction.class);
                    IncomeOperation enrollmentOperation = argument.getEnrollmentOperation();
                    OutcomeOperation withdrawalOperation = argument.getWithdrawalOperation();
                    return new TransactionDetails().setId(argument.getId())
                            .setAmount(enrollmentOperation.getAmount())
                            .setType(TRANSFER)
                            .setOutcomeAccountId(withdrawalOperation.getAccount().getId())
                            .setIncomeAccountId(enrollmentOperation.getAccount().getId())
                            .setTimestamp(argument.getTimestamp());
                });

        IncomeOperation incomeOperation = new IncomeOperation()
                .setTransaction(transferTransaction)
                .setAmount(BigDecimal.TEN)
                .setAccount(new Account().setId(1L)
                        .setAmount(BigDecimal.ZERO));

        OutcomeOperation outcomeOperation = new OutcomeOperation()
                .setTransaction(transferTransaction)
                .setAmount(BigDecimal.TEN)
                .setAccount(new Account().setId(2L)
                        .setAmount(BigDecimal.TEN));

        transferTransaction.setWithdrawalOperation(outcomeOperation);
        transferTransaction.setEnrollmentOperation(incomeOperation);

        TransactionDetails actual = subject.execute(transferTransaction);

        assertNotNull(actual);
        assertEquals(1L, actual.getId());
        assertEquals(BigDecimal.TEN, actual.getAmount());
        assertEquals(LocalDateTime.MAX, actual.getTimestamp());
        assertEquals(TRANSFER, actual.getType());
        assertEquals(1L, actual.getIncomeAccountId());
        assertEquals(2L, actual.getOutcomeAccountId());

        verify(accountDao, times(1)).income(1L, BigDecimal.TEN);
        verify(accountDao, times(1)).outcome(2L, BigDecimal.TEN);
    }

    @Test
    void execute_outcomeTransaction() {
        mockDbGeneratedValue(1L);
        OutcomeTransaction outcomeTransaction = new OutcomeTransaction();
        when(outcomeTransactionTransactionDetailsConverter.convert(any(OutcomeTransaction.class))).
                thenAnswer(invocationOnMock -> {
                    OutcomeTransaction argument = invocationOnMock.getArgument(0, OutcomeTransaction.class);
                    OutcomeOperation outcomeOperation = argument.getOutcomeOperation();
                    return new TransactionDetails()
                            .setId(argument.getId())
                            .setAmount(outcomeOperation.getAmount())
                            .setType(OUTCOME)
                            .setOutcomeAccountId(outcomeOperation.getAccount().getId())
                            .setTimestamp(argument.getTimestamp());
                });

        OutcomeOperation outcomeOperation = new OutcomeOperation()
                .setTransaction(outcomeTransaction)
                .setAmount(BigDecimal.TEN)
                .setAccount(new Account().setId(2L)
                        .setAmount(BigDecimal.TEN));

        outcomeTransaction.setOutcomeOperation(outcomeOperation);

        TransactionDetails actual = subject.execute(outcomeTransaction);

        assertNotNull(actual);
        assertEquals(1L, actual.getId());
        assertEquals(BigDecimal.TEN, actual.getAmount());
        assertEquals(LocalDateTime.MAX, actual.getTimestamp());
        assertEquals(OUTCOME, actual.getType());
        assertEquals(2L, actual.getOutcomeAccountId());
    }
}