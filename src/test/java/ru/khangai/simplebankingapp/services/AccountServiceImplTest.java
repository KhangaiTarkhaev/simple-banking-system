package ru.khangai.simplebankingapp.services;

import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import ru.khangai.simplebankingapp.converters.AccountToDtoConverter;
import ru.khangai.simplebankingapp.dao.AccountDao;
import ru.khangai.simplebankingapp.dao.CounterpartyDao;
import ru.khangai.simplebankingapp.dto.AccountDto;
import ru.khangai.simplebankingapp.entities.Account;
import ru.khangai.simplebankingapp.entities.PersonDetails;
import ru.khangai.simplebankingapp.entities.User;

import java.math.BigDecimal;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.comparesEqualTo;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class AccountServiceImplTest {

    @InjectMocks
    AccountServiceImpl subject;

    @Mock
    AccountDao accountDao;

    @Mock
    CounterpartyDao counterpartyDao;

    @Spy
    AccountToDtoConverter converter = new AccountToDtoConverter();

    @Test
    void createNewAccount() {
        PersonDetails personDetails = new PersonDetails()
                .setFirstName("newFirstName")
                .setLastName("newLastName")
                .setEmail("newEmail")
                .setPhoneNumber("someNumber");

        User user = new User();
        user.setId(1L);
        user.setDetails(personDetails);

        when(counterpartyDao.findById(1L)).thenReturn(user);
        when(accountDao.save(any(Account.class))).thenAnswer(invocationOnMock -> {
            Account argument = invocationOnMock.getArgument(0, Account.class);
            argument.setId(1L);
            return argument;
        });

        AccountDto actual = subject.createNewAccount(1L);

        assertNotNull(actual);
        assertEquals(1L, actual.getId());
        assertThat(BigDecimal.ZERO, comparesEqualTo(actual.getAmount()));
        assertEquals(1L, actual.getCounterpartyId());
    }
}