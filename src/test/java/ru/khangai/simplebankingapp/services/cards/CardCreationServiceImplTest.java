package ru.khangai.simplebankingapp.services.cards;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import ru.khangai.simplebankingapp.dao.CardDao;
import ru.khangai.simplebankingapp.dao.UserDao;
import ru.khangai.simplebankingapp.dto.cards.CardCreationCredentials;
import ru.khangai.simplebankingapp.entities.Account;
import ru.khangai.simplebankingapp.entities.Card;
import ru.khangai.simplebankingapp.entities.PersonDetails;
import ru.khangai.simplebankingapp.entities.User;
import ru.khangai.simplebankingapp.services.AccountService;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class CardCreationServiceImplTest {

    @InjectMocks
    CardCreationServiceImpl subject;

    @Mock
    AccountService accountService;

    @Mock
    UserDao userDao;

    @Mock
    CardDao cardDao;

    @Test
    void createNewCard() {
        PersonDetails personDetails = new PersonDetails()
                .setEmail("khangai@mail.ru")
                .setFirstName("khangai")
                .setLastName("tarkhaev")
                .setPhoneNumber("1234");

        User user = new User();
        user.setId(1L);
        user.setDetails(personDetails);

        Account account = new Account()
                .setId(1L)
                .setCounterparty(user);

        when(accountService.getById(1L)).thenReturn(account);

        when(userDao.findById(1L)).thenReturn(user);

        when(cardDao.save(any(Card.class))).thenAnswer(invocationOnMock -> {
            Card argument = invocationOnMock.getArgument(0, Card.class);
            argument.setId(1L);
            return argument;
        });

        Card actual = subject.createNewCard(new CardCreationCredentials().setAccountId(1L));

        assertNotNull(actual);
        assertEquals(account, actual.getAccount());
        assertTrue(actual.getCvcCode().matches("[0-9]{3}"));
        assertTrue(actual.getCardNumber().matches("[0-9]{16}"));
        assertEquals("KHANGAI TARKHAEV", actual.getCardHolderName());
    }
}