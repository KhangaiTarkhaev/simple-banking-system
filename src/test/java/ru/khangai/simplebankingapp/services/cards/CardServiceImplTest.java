package ru.khangai.simplebankingapp.services.cards;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import ru.khangai.simplebankingapp.converters.CardToDtoConverter;
import ru.khangai.simplebankingapp.dao.CardDao;
import ru.khangai.simplebankingapp.dto.cards.CardAmountDto;
import ru.khangai.simplebankingapp.dto.cards.CardDto;
import ru.khangai.simplebankingapp.dto.cards.CardOperationCredentials;
import ru.khangai.simplebankingapp.dto.transactions.ExternalIncomeTransactionCredentials;
import ru.khangai.simplebankingapp.dto.transactions.ExternalOutcomeTransactionCredentials;
import ru.khangai.simplebankingapp.dto.transactions.TransactionDetails;
import ru.khangai.simplebankingapp.dto.transactions.TransferTransactionCredentials;
import ru.khangai.simplebankingapp.entities.Account;
import ru.khangai.simplebankingapp.entities.Card;
import ru.khangai.simplebankingapp.entities.User;
import ru.khangai.simplebankingapp.services.transactions.TransactionService;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static ru.khangai.simplebankingapp.dto.cards.CardOperationCredentials.CardOperationType.*;

@ExtendWith(MockitoExtension.class)
class CardServiceImplTest {

    @InjectMocks
    CardServiceImpl subject;

    @Mock
    CardCreationService cardCreationService;

    @Spy
    CardToDtoConverter cardToDtoConverter = new CardToDtoConverter();

    @Mock
    CardDao cardDao;

    @Mock
    TransactionService transactionService;

    CardDto testCardDto;

    Card testCard;

    @BeforeEach
    void setUp() {
        User user = new User();
        user.setId(1L);

        testCard = new Card()
                .setCvcCode("123")
                .setCardNumber("1234567891234567")
                .setCardHolder(user)
                .setCardHolderName("KHANGAI TARKHAEV")
                .setId(1L).setAccount(new Account().setId(1L)
                        .setAmount(BigDecimal.TEN));

        testCardDto = new CardDto()
                .setCardNumber("1234567891234567")
                .setCvcCode("123")
                .setCardHolderName("KHANGAI TARKHAEV");
    }

    @AfterEach
    void tearDown() {
        testCardDto = null;
    }

    @Test
    void getAllCardsByUserId() {
        List<Card> cards = new ArrayList<>();
        cards.add(testCard);

        when(cardDao.getAllCardsByUserId(1L)).thenReturn(cards);

        List<CardDto> cardsByUserId = subject.getAllCardsByUserId(1L);

        assertTrue(cardsByUserId.contains(testCardDto));
    }

    @Test
    void getCardBalance() {
        User user = new User();
        user.setId(1L);

        when(cardDao.findByCardNumber(testCard.getCardNumber())).thenReturn(testCard);

        CardAmountDto cardBalance = subject.getCardBalance(testCardDto);

        assertNotNull(cardBalance);
        assertEquals(BigDecimal.TEN, cardBalance.getAmount());
    }

    @Test
    void deposit() {
        when(cardDao.findByCardNumber(testCardDto.getCardNumber())).thenReturn(testCard);

        when(transactionService.proceedTransaction(any(ExternalIncomeTransactionCredentials.class)))
                .thenReturn(new TransactionDetails().setId(1L));

        subject.deposit(new CardOperationCredentials()
                .setAmount(BigDecimal.TEN)
                .setType(DEPOSIT)
                .setEnrollmentCard(testCardDto));

        verify(transactionService, times(1)).proceedTransaction(new ExternalIncomeTransactionCredentials()
                .setEnrollmentAccountId(1L)
                .setAmount(BigDecimal.TEN));
    }

    @Test
    void withdraw() {
        when(cardDao.findByCardNumber(testCardDto.getCardNumber())).thenReturn(testCard);

        when(transactionService.proceedTransaction(any(ExternalOutcomeTransactionCredentials.class)))
                .thenReturn(new TransactionDetails().setId(1L));

        subject.withdraw(new CardOperationCredentials()
                .setAmount(BigDecimal.TEN)
                .setType(WITHDRAW)
                .setWithdrawalCard(testCardDto));

        verify(transactionService, times(1)).proceedTransaction(new ExternalOutcomeTransactionCredentials()
                .setWithdrawalAccountId(1L)
                .setAmount(BigDecimal.TEN));
    }

    @Test
    void transfer() {
        when(cardDao.findByCardNumber(testCardDto.getCardNumber())).thenReturn(testCard);

        when(transactionService.proceedTransaction(any(TransferTransactionCredentials.class)))
                .thenReturn(new TransactionDetails().setId(1L));

        Card enrollmentCard = new Card()
                .setCardNumber("987654321")
                .setCardHolderName("Alexander Pushkin")
                .setAccount(new Account().setAmount(BigDecimal.TEN)
                        .setId(2L))
                .setCvcCode("321");

        CardDto enrollmentCardDto = new CardDto()
                .setCardNumber("987654321")
                .setCardHolderName("Alexander Pushkin")
                .setCvcCode("321");

        when(cardDao.findByCardNumber(enrollmentCardDto.getCardNumber())).thenReturn(enrollmentCard);

        subject.transfer(new CardOperationCredentials()
                .setAmount(BigDecimal.TEN)
                .setType(TRANSFER)
                .setWithdrawalCard(testCardDto)
                .setEnrollmentCard(enrollmentCardDto));

        verify(transactionService, times(1)).proceedTransaction(new TransferTransactionCredentials()
                .setWithdrawalAccountId(1L)
                .setEnrollmentAccountId(2L)
                .setAmount(BigDecimal.TEN));
    }
}