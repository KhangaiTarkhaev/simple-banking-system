package ru.khangai.simplebankingapp.services.cards;

import org.junit.jupiter.api.Test;
import ru.khangai.simplebankingapp.dao.AbstractDaoIT;

import static org.junit.jupiter.api.Assertions.*;

class RandomDigitsGeneratorTest {

    @Test
    void generateRandomDigits() {
        long number = RandomDigitsGenerator.generateRandomDigits(16);
        String numberString = String.valueOf(number);

        assertEquals(16, numberString.length());

        long cvc = RandomDigitsGenerator.generateRandomDigits(3);
        String cvcString = String.valueOf(cvc);

        assertEquals(3, cvcString.length());
    }
}