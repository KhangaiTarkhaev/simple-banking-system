package ru.khangai.simplebankingapp.dto.validators;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import ru.khangai.simplebankingapp.dto.cards.CardDto;
import ru.khangai.simplebankingapp.dto.cards.CardOperationCredentials;

import javax.validation.ConstraintValidatorContext;
import javax.validation.ConstraintValidatorContext.ConstraintViolationBuilder;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;
import static ru.khangai.simplebankingapp.dto.cards.CardOperationCredentials.CardOperationType.*;

class CardOperationCredentialsValidatorTest {

    CardOperationCredentialsValidator subject;

    ConstraintValidatorContext constraintValidatorContext;

    ConstraintViolationBuilder constraintViolationBuilder;

    @BeforeEach
    void setUp() {
        subject = new CardOperationCredentialsValidator();
        constraintValidatorContext = mock(ConstraintValidatorContext.class);
        constraintViolationBuilder = mock(ConstraintViolationBuilder.class);
    }

    @AfterEach
    void tearDown() {
        subject = null;
    }

    @Test
    void isValid_deposit_valid() {
        CardOperationCredentials cardOperationCredentials = new CardOperationCredentials()
                .setType(DEPOSIT)
                .setEnrollmentCard(new CardDto()
                        .setCardNumber("1234567891234567")
                        .setCardHolderName("KHANGAI TARKHAEV")
                        .setCvcCode("123"));

        boolean valid = subject.isValid(cardOperationCredentials, constraintValidatorContext);

        assertTrue(valid);

        verifyNoInteractions(constraintValidatorContext);
    }

    @Test
    void isValid_deposit_invalid() {
        when(constraintValidatorContext.buildConstraintViolationWithTemplate(anyString())).thenReturn(constraintViolationBuilder);

        CardOperationCredentials cardOperationCredentials = new CardOperationCredentials()
                .setType(DEPOSIT)
                .setEnrollmentCard(null);

        boolean valid = subject.isValid(cardOperationCredentials, constraintValidatorContext);

        assertFalse(valid);

        verify(constraintValidatorContext, times(1)).buildConstraintViolationWithTemplate("when operation type is DEPOSIT enrollment card must not be null");
        verify(constraintViolationBuilder, times(1)).addConstraintViolation();
    }

    @Test
    void isValid_withdraw_valid() {
        CardOperationCredentials cardOperationCredentials = new CardOperationCredentials()
                .setType(WITHDRAW)
                .setWithdrawalCard(new CardDto()
                        .setCardNumber("1234567891234567")
                        .setCardHolderName("KHANGAI TARKHAEV")
                        .setCvcCode("123"));

        boolean valid = subject.isValid(cardOperationCredentials, constraintValidatorContext);

        assertTrue(valid);

        verifyNoInteractions(constraintValidatorContext);
    }

    @Test
    void isValid_withdraw_invalid() {
        when(constraintValidatorContext.buildConstraintViolationWithTemplate(anyString())).thenReturn(constraintViolationBuilder);

        CardOperationCredentials cardOperationCredentials = new CardOperationCredentials()
                .setType(WITHDRAW)
                .setWithdrawalCard(null);

        boolean valid = subject.isValid(cardOperationCredentials, constraintValidatorContext);

        assertFalse(valid);

        verify(constraintValidatorContext, times(1)).buildConstraintViolationWithTemplate("when operation type is WITHDRAW withdrawal card must not be null");
        verify(constraintViolationBuilder, times(1)).addConstraintViolation();
    }

    @Test
    void isValid_transfer_valid() {
        CardOperationCredentials cardOperationCredentials = new CardOperationCredentials()
                .setType(WITHDRAW)
                .setWithdrawalCard(new CardDto()
                        .setCardNumber("1234567891234567")
                        .setCardHolderName("KHANGAI TARKHAEV")
                        .setCvcCode("123"))
                .setEnrollmentCard(new CardDto()
                        .setCardNumber("9876543219876543")
                        .setCardHolderName("KHANGAI TARKHAEV")
                        .setCvcCode("321"));

        boolean valid = subject.isValid(cardOperationCredentials, constraintValidatorContext);

        assertTrue(valid);

        verifyNoInteractions(constraintValidatorContext);
    }

    @Test
    void isValid_transfer_invalid() {
        when(constraintValidatorContext.buildConstraintViolationWithTemplate(anyString())).thenReturn(constraintViolationBuilder);

        CardOperationCredentials cardOperationCredentials = new CardOperationCredentials()
                .setType(TRANSFER)
                .setWithdrawalCard(null)
                .setEnrollmentCard(null);

        boolean valid = subject.isValid(cardOperationCredentials, constraintValidatorContext);

        assertFalse(valid);

        verify(constraintValidatorContext, times(1)).buildConstraintViolationWithTemplate("when operation type is TRANSFER both cards must not be null");
        verify(constraintViolationBuilder, times(1)).addConstraintViolation();
    }
}