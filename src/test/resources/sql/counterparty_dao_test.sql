INSERT INTO USERS
VALUES (1, 'khangai@mail.ru', 'khangai', 'tarkhaev', '+12345678', 0),
       (2, 'alexanderpushkin@mail.ru', 'alexander', 'pushkin', '+98765432', 0),
       (3, 'geraltofrivia@mail.ru', 'Geralt', 'Of Rivia', '+1010101010', 0);

INSERT INTO COUNTERPARTY_TO_COUNTERPARTY
VALUES (1, 2),
       (2, 1);